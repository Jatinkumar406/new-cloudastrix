<?php

namespace Modules\DoubleEntry\Providers;

use App\Events\AdminMenuCreated;
use App\Events\ModuleInstalled;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\DoubleEntry\Listeners\DoubleEntryAdminMenu;
use Modules\DoubleEntry\Listeners\DoubleEntryModuleInstalled;

class DoubleEntryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerMigrations();
        $this->registerFactories();
        $this->registerEvents();
        $this->registerCommands();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('doubleentry.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'doubleentry'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/doubleentry');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/doubleentry';
        }, \Config::get('view.paths')), [$sourcePath]), 'doubleentry');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/doubleentry');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'doubleentry');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'doubleentry');
        }
    }

    public function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/Database/factories');
        }
    }

    public function registerEvents()
    {
        $this->app['events']->listen(AdminMenuCreated::class, DoubleEntryAdminMenu::class);
        $this->app['events']->listen(ModuleInstalled::class, DoubleEntryModuleInstalled::class);
    }

    public function registerCommands()
    {
        $this->commands(\Modules\DoubleEntry\Console\DoubleEntrySeed::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
