<?php

namespace Modules\DoubleEntry\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Invoice
        View::composer(
            ['incomes.invoices.create', 'incomes.invoices.edit'], 'Modules\DoubleEntry\Http\ViewComposers\Invoice'
        );

        // Bill
        View::composer(
            ['expenses.bills.create', 'expenses.bills.edit'], 'Modules\DoubleEntry\Http\ViewComposers\Bill'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
