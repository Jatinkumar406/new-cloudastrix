<?php

namespace Modules\DoubleEntry\Providers;

use App\Models\Banking\Account;
use App\Models\Company\Company;
use App\Models\Expense\Bill;
use App\Models\Expense\BillItem;
use App\Models\Expense\BillPayment;
use App\Models\Expense\Payment;
use App\Models\Income\Invoice;
use App\Models\Income\InvoiceItem;
use App\Models\Income\InvoicePayment;
use App\Models\Income\Revenue;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        Account::observe('Modules\DoubleEntry\Observers\Banking\Account');
        //Company::observe('Modules\DoubleEntry\Observers\Company\Company');
        Bill::observe('Modules\DoubleEntry\Observers\Expense\Bill');
        BillItem::observe('Modules\DoubleEntry\Observers\Expense\BillItem');
        BillPayment::observe('Modules\DoubleEntry\Observers\Expense\BillPayment');
        Payment::observe('Modules\DoubleEntry\Observers\Expense\Payment');
        Invoice::observe('Modules\DoubleEntry\Observers\Income\Invoice');
        InvoiceItem::observe('Modules\DoubleEntry\Observers\Income\InvoiceItem');
        InvoicePayment::observe('Modules\DoubleEntry\Observers\Income\InvoicePayment');
        Revenue::observe('Modules\DoubleEntry\Observers\Income\Revenue');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}