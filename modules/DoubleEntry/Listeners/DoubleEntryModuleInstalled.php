<?php

namespace Modules\DoubleEntry\Listeners;

use App\Events\ModuleInstalled;
use App\Models\Auth\Role;
use App\Models\Auth\Permission;
use App\Models\Banking\Account;
use App\Models\Income\Invoice;
use App\Models\Income\InvoiceItem;
use App\Models\Income\Revenue;
use App\Models\Expense\Bill;
use App\Models\Expense\BillItem;
use App\Models\Expense\Payment;
use Artisan;
use Modules\DoubleEntry\Models\Account as ChartAccount;
use Modules\DoubleEntry\Models\AccountBank;
use Modules\DoubleEntry\Models\Ledger;

class DoubleEntryModuleInstalled
{
    public $company_id;
    
    /**
     * Handle the event.
     *
     * @param  ModuleInstalled $event
     * @return void
     */
    public function handle(ModuleInstalled $event)
    {
        if ($event->alias != 'double-entry') {
            return;
        }
        
        $this->company_id = $event->company_id;

        // Create seeds
        Artisan::call('doubleentry:seed', [
            'company' => $event->company_id
        ]);

        // Update permissions
        $this->updatePermissions();

        // Copy current data
        $this->copyData();
    }

    protected function updatePermissions()
    {
        // Check if already exists
        if ($p = Permission::where('name', 'read-double-entry-balance-sheet')->value('id')) {
            return;
        }

        $permissions = [];

        $permissions[] = Permission::firstOrCreate([
            'name' => 'read-double-entry-balance-sheet',
            'display_name' => 'Read Double-Entry Balance Sheet',
            'description' => 'Read Double-Entry Balance Sheet',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'create-double-entry-chart-of-accounts',
            'display_name' => 'Create Double-Entry Chart of Accounts',
            'description' => 'Create Double-Entry Chart of Accounts',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'read-double-entry-chart-of-accounts',
            'display_name' => 'Read Double-Entry Chart of Accounts',
            'description' => 'Read Double-Entry Chart of Accounts',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'update-double-entry-chart-of-accounts',
            'display_name' => 'Update Double-Entry Chart of Accounts',
            'description' => 'Update Double-Entry Chart of Accounts',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'delete-double-entry-chart-of-accounts',
            'display_name' => 'Delete Double-Entry Chart of Accounts',
            'description' => 'Delete Double-Entry Chart of Accounts',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'read-double-entry-general-ledger',
            'display_name' => 'Read Double-Entry General Ledger',
            'description' => 'Read Double-Entry General Ledger',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'create-double-entry-journal-entry',
            'display_name' => 'Create Double-Entry Journal Entry',
            'description' => 'Create Double-Entry Journal Entry',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'read-double-entry-journal-entry',
            'display_name' => 'Read Double-Entry Journal Entry',
            'description' => 'Read Double-Entry Journal Entry',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'update-double-entry-journal-entry',
            'display_name' => 'Update Double-Entry Journal Entry',
            'description' => 'Update Double-Entry Journal Entry',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'delete-double-entry-journal-entry',
            'display_name' => 'Delete Double-Entry Journal Entry',
            'description' => 'Delete Double-Entry Journal Entry',
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'read-double-entry-trial-balance',
            'display_name' => 'Read Double-Entry Trial Balance',
            'description' => 'Read Double-Entry Trial Balance',
        ]);

        // Attach permission to roles
        $roles = Role::all();

        foreach ($roles as $role) {
            $allowed = ['admin', 'manager'];

            if (!in_array($role->name, $allowed)) {
                continue;
            }

            foreach ($permissions as $permission) {
                $role->attachPermission($permission);
            }
        }
    }

    protected function copyData()
    {
        $this->copyAccounts();
        $this->copyInvoices();
        $this->copyRevenues();
        $this->copyBills();
        $this->copyPayments();
    }

    protected function copyAccounts()
    {
        Account::all()->each(function ($bank) {
            $account_code = ChartAccount::max('code') + 1;

            $chart = ChartAccount::create([
                'company_id' => $this->company_id,
                'type_id' => 6,
                'code' => $account_code,
                'name' => $bank->name,
                'system' => 0,
                'enabled' => 1,
            ]);

            AccountBank::create([
                'company_id' => $this->company_id,
                'account_id' => $chart->id,
                'bank_id' => $bank->id,
            ]);
        });
    }

    protected function copyInvoices()
    {
        Invoice::with(['items', 'payments'])->get()->each(function ($invoice) {
            $accounts_receivable_id = ChartAccount::code('120')->value('id');

            $ledger = Ledger::create([
                'company_id' => $this->company_id,
                'account_id' => $accounts_receivable_id,
                'ledgerable_id' => $invoice->id,
                'ledgerable_type' => get_class($invoice),
                'issued_at' => $invoice->invoiced_at,
                'entry_type' => 'total',
                'debit' => $invoice->amount,
            ]);

            $invoice->items()->each(function ($item) use($invoice) {
                $account_id = ChartAccount::code('400')->value('id');

                $ledger = Ledger::create([
                    'company_id' => $this->company_id,
                    'account_id' => $account_id,
                    'ledgerable_id' => $item->id,
                    'ledgerable_type' => get_class($item),
                    'issued_at' => $invoice->invoiced_at,
                    'entry_type' => 'item',
                    'credit' => $item->total,
                ]);
            });

            $invoice->payments()->each(function ($payment) use($accounts_receivable_id) {
                $account_id = AccountBank::where('bank_id', $payment->account_id)->value('account_id');

                $ledger = Ledger::create([
                    'company_id' => $this->company_id,
                    'account_id' => $account_id,
                    'ledgerable_id' => $payment->id,
                    'ledgerable_type' => get_class($payment),
                    'issued_at' => $payment->paid_at,
                    'entry_type' => 'total',
                    'debit' => $payment->amount,
                ]);

                $ledger = Ledger::create([
                    'company_id' => $this->company_id,
                    'account_id' => $accounts_receivable_id,
                    'ledgerable_id' => $payment->id,
                    'ledgerable_type' => get_class($payment),
                    'issued_at' => $payment->paid_at,
                    'entry_type' => 'item',
                    'credit' => $payment->amount,
                ]);
            });

        });
    }

    protected function copyRevenues()
    {
        Revenue::all()->each(function ($revenue) {
            $account_id = AccountBank::where('bank_id', $revenue->account_id)->value('account_id');

            $ledger = Ledger::create([
                'company_id' => $this->company_id,
                'account_id' => $account_id,
                'ledgerable_id' => $revenue->id,
                'ledgerable_type' => get_class($revenue),
                'issued_at' => $revenue->paid_at,
                'entry_type' => 'total',
                'debit' => $revenue->amount,
            ]);

            $accounts_receivable_id = ChartAccount::code('120')->value('id');

            $ledger = Ledger::create([
                'company_id' => $this->company_id,
                'account_id' => $accounts_receivable_id,
                'ledgerable_id' => $revenue->id,
                'ledgerable_type' => get_class($revenue),
                'issued_at' => $revenue->paid_at,
                'entry_type' => 'item',
                'credit' => $revenue->amount,
            ]);
        });
    }

    protected function copyBills()
    {
        Bill::with(['items', 'payments'])->get()->each(function ($bill) {
            $accounts_payable_id = ChartAccount::code('200')->value('id');

            $ledger = Ledger::create([
                'company_id' => $this->company_id,
                'account_id' => $accounts_payable_id,
                'ledgerable_id' => $bill->id,
                'ledgerable_type' => get_class($bill),
                'issued_at' => $bill->billed_at,
                'entry_type' => 'total',
                'credit' => $bill->amount,
            ]);

            $bill->items()->each(function ($item) use($bill) {
                $account_id = ChartAccount::code('640')->value('id');

                $ledger = Ledger::create([
                    'company_id' => $this->company_id,
                    'account_id' => $account_id,
                    'ledgerable_id' => $item->id,
                    'ledgerable_type' => get_class($item),
                    'issued_at' => $bill->billed_at,
                    'entry_type' => 'item',
                    'debit' => $item->total,
                ]);
            });

            $bill->payments()->each(function ($payment) use($accounts_payable_id) {
                $account_id = AccountBank::where('bank_id', $payment->account_id)->value('account_id');

                $ledger = Ledger::create([
                    'company_id' => $this->company_id,
                    'account_id' => $account_id,
                    'ledgerable_id' => $payment->id,
                    'ledgerable_type' => get_class($payment),
                    'issued_at' => $payment->paid_at,
                    'entry_type' => 'total',
                    'credit' => $payment->amount,
                ]);

                $ledger = Ledger::create([
                    'company_id' => $this->company_id,
                    'account_id' => $accounts_payable_id,
                    'ledgerable_id' => $payment->id,
                    'ledgerable_type' => get_class($payment),
                    'issued_at' => $payment->paid_at,
                    'entry_type' => 'item',
                    'debit' => $payment->amount,
                ]);
            });

        });
    }

    protected function copyPayments()
    {
        Payment::all()->each(function ($payment) {
            $account_id = AccountBank::where('bank_id', $payment->account_id)->value('account_id');

            $ledger = Ledger::create([
                'company_id' => $this->company_id,
                'account_id' => $account_id,
                'ledgerable_id' => $payment->id,
                'ledgerable_type' => get_class($payment),
                'issued_at' => $payment->paid_at,
                'entry_type' => 'total',
                'credit' => $payment->amount,
            ]);

            $accounts_payable_id = ChartAccount::code('200')->value('id');

            $ledger = Ledger::create([
                'company_id' => $this->company_id,
                'account_id' => $accounts_payable_id,
                'ledgerable_id' => $payment->id,
                'ledgerable_type' => get_class($payment),
                'issued_at' => $payment->paid_at,
                'entry_type' => 'item',
                'debit' => $payment->amount,
            ]);
        });
    }
}