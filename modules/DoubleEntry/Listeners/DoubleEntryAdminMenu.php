<?php

namespace Modules\DoubleEntry\Listeners;

use App\Events\AdminMenuCreated;

class DoubleEntryAdminMenu
{
    /**
     * Handle the event.
     *
     * @param  AdminMenuCreated $event
     * @return void
     */
    public function handle(AdminMenuCreated $event)
    {
        $user = auth()->user();

        if (!$user->can([
            'read-double-entry-chart-of-accounts',
            'read-double-entry-journal-entry',
            'read-double-entry-general-ledger',
            'read-double-entry-balance-sheet',
            'read-double-entry-trial-balance',
        ])) {
            return;
        }

        $attr = ['icon' => 'fa fa-angle-double-right'];

        $event->menu->dropdown(trans('doubleentry::general.double_entry'), function ($sub) use($user, $attr) {
            if ($user->can('read-double-entry-chart-of-accounts')) {
                $sub->url('double-entry/chart-of-accounts', trans('doubleentry::general.chart_of_accounts'), 1, $attr);
            }

            if ($user->can('read-double-entry-journal-entry')) {
                $sub->url('double-entry/journal-entry', trans('doubleentry::general.journal_entry'), 2, $attr);
            }

            if ($user->can('read-double-entry-general-ledger')) {
                $sub->url('double-entry/general-ledger', trans('doubleentry::general.general_ledger'), 3, $attr);
            }

            if ($user->can('read-double-entry-balance-sheet')) {
                $sub->url('double-entry/balance-sheet', trans('doubleentry::general.balance_sheet'), 4, $attr);
            }

            if ($user->can('read-double-entry-trial-balance')) {
                $sub->url('double-entry/trial-balance', trans('doubleentry::general.trial_balance'), 5, $attr);
            }
        }, 6, [
            'title' => trans('doubleentry::general.double_entry'),
            'icon' => 'fa fa-balance-scale',
        ]);
    }
}