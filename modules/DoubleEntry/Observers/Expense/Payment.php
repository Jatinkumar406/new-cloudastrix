<?php

namespace Modules\DoubleEntry\Observers\Expense;

use App\Models\Expense\Payment as Model;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\AccountBank;
use Modules\DoubleEntry\Models\Ledger;

class Payment
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $payment
     * @return void
     */
    public function created(Model $payment)
    {
        $account_id = AccountBank::where('bank_id', $payment->account_id)->value('account_id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $account_id,
            'ledgerable_id' => $payment->id,
            'ledgerable_type' => get_class($payment),
            'issued_at' => $payment->paid_at,
            'entry_type' => 'total',
            'credit' => $payment->amount,
        ]);

        $accounts_payable_id = Account::code('200')->value('id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $accounts_payable_id,
            'ledgerable_id' => $payment->id,
            'ledgerable_type' => get_class($payment),
            'issued_at' => $payment->paid_at,
            'entry_type' => 'item',
            'debit' => $payment->amount,
        ]);
    }

    /**
     * Listen to the created event.
     *
     * @param  Model  $payment
     * @return void
     */
    public function updated(Model $payment)
    {
        $ledger = Ledger::record($payment->id, get_class($payment))->where('entry_type', 'total')->first();

        $account_id = AccountBank::where('bank_id', $payment->account_id)->value('account_id');

        $ledger->update([
            'company_id' => session('company_id'),
            'account_id' => $account_id,
            'ledgerable_id' => $payment->id,
            'ledgerable_type' => get_class($payment),
            'issued_at' => $payment->paid_at,
            'entry_type' => 'total',
            'credit' => $payment->amount,
        ]);

        $ledger = Ledger::record($payment->id, get_class($payment))->where('entry_type', 'item')->first();

        $accounts_payable_id = Account::code('200')->value('id');

        $ledger->update([
            'company_id' => session('company_id'),
            'account_id' => $accounts_payable_id,
            'ledgerable_id' => $payment->id,
            'ledgerable_type' => get_class($payment),
            'issued_at' => $payment->paid_at,
            'entry_type' => 'item',
            'debit' => $payment->amount,
        ]);
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $payment
     * @return void
     */
    public function deleted(Model $payment)
    {
        Ledger::record($payment->id, get_class($payment))->delete();
    }
}