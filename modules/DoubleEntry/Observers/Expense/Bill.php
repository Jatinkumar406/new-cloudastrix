<?php

namespace Modules\DoubleEntry\Observers\Expense;

use App\Models\Expense\Bill as Model;
use App\Models\Expense\BillItem;
use App\Models\Expense\BillPayment;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\Ledger;

class Bill
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $bill
     * @return void
     */
    public function created(Model $bill)
    {
        $accounts_payable_id = Account::code('200')->value('id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $accounts_payable_id,
            'ledgerable_id' => $bill->id,
            'ledgerable_type' => get_class($bill),
            'issued_at' => $bill->billed_at,
            'entry_type' => 'total',
            'credit' => $bill->amount,
        ]);
    }

    /**
     * Listen to the created event.
     *
     * @param  Model  $bill
     * @return void
     */
    public function updated(Model $bill)
    {
        $ledger = Ledger::record($bill->id, get_class($bill))->first();

        $accounts_payable_id = Account::code('200')->value('id');

        $ledger->update([
            'company_id' => session('company_id'),
            'account_id' => $accounts_payable_id,
            'ledgerable_id' => $bill->id,
            'ledgerable_type' => get_class($bill),
            'issued_at' => $bill->billed_at,
            'entry_type' => 'total',
            'credit' => $bill->amount,
        ]);
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $bill
     * @return void
     */
    public function deleted(Model $bill)
    {
        Ledger::record($bill->id, get_class($bill))->delete();

        BillItem::where('bill_id', $bill->id)->get()->each(function ($item) {
            Ledger::record($item->id, get_class($item))->delete();
        });

        BillPayment::where('bill_id', $bill->id)->get()->each(function ($item) {
            Ledger::record($item->id, get_class($item))->delete();
        });
    }
}