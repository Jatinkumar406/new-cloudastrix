<?php

namespace Modules\DoubleEntry\Observers\Banking;

use App\Models\Banking\Account as Model;
use Modules\DoubleEntry\Models\Account as Chart;
use Modules\DoubleEntry\Models\AccountBank;

class Account
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $bank
     * @return void
     */
    public function created(Model $bank)
    {
        $account_code = Chart::max('code') + 1;

        $account = Chart::create([
            'company_id' => session('company_id'),
            'type_id' => 6,
            'code' => $account_code,
            'name' => $bank->name,
            'system' => 0,
            'enabled' => 1,
        ]);

        AccountBank::create([
            'company_id' => session('company_id'),
            'account_id' => $account->id,
            'bank_id' => $bank->id,
        ]);
    }

    /**
     * Listen to the created event.
     *
     * @param  Model  $bank
     * @return void
     */
    public function updated(Model $bank)
    {
        $bank = AccountBank::where('bank_id', $bank->id)->first();

        if (!$bank) {
            return;
        }

        $bank->account()->update([
            'name' => $bank->name,
        ]);
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $bank
     * @return void
     */
    public function deleted(Model $bank)
    {
        $bank = AccountBank::where('bank_id', $bank->id)->first();

        if (!$bank) {
            return;
        }

        $bank->account()->delete();
    }
}