<?php

namespace Modules\DoubleEntry\Observers\Income;

use App\Models\Income\Invoice as Model;
use App\Models\Income\InvoiceItem;
use App\Models\Income\InvoicePayment;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\Ledger;

class Invoice
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $invoice
     * @return void
     */
    public function created(Model $invoice)
    {
        $accounts_receivable_id = Account::code('120')->value('id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $accounts_receivable_id,
            'ledgerable_id' => $invoice->id,
            'ledgerable_type' => get_class($invoice),
            'issued_at' => $invoice->invoiced_at,
            'entry_type' => 'total',
            'debit' => $invoice->amount,
        ]);
    }

    /**
     * Listen to the created event.
     *
     * @param  Model  $invoice
     * @return void
     */
    public function updated(Model $invoice)
    {
        $ledger = Ledger::record($invoice->id, get_class($invoice))->first();

        $accounts_receivable_id = Account::code('120')->value('id');

        $ledger->update([
            'company_id' => session('company_id'),
            'account_id' => $accounts_receivable_id,
            'ledgerable_id' => $invoice->id,
            'ledgerable_type' => get_class($invoice),
            'issued_at' => $invoice->invoiced_at,
            'entry_type' => 'total',
            'debit' => $invoice->amount,
        ]);
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $invoice
     * @return void
     */
    public function deleted(Model $invoice)
    {
        Ledger::record($invoice->id, get_class($invoice))->delete();

        InvoiceItem::where('invoice_id', $invoice->id)->get()->each(function ($item) {
            Ledger::record($item->id, get_class($item))->delete();
        });

        InvoicePayment::where('invoice_id', $invoice->id)->get()->each(function ($item) {
            Ledger::record($item->id, get_class($item))->delete();
        });
    }
}