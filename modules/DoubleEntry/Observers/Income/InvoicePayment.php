<?php

namespace Modules\DoubleEntry\Observers\Income;

use App\Models\Income\InvoicePayment as Model;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\AccountBank;
use Modules\DoubleEntry\Models\Ledger;

class InvoicePayment
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $payment
     * @return void
     */
    public function created(Model $payment)
    {
        $account_id = AccountBank::where('bank_id', $payment->account_id)->value('account_id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $account_id,
            'ledgerable_id' => $payment->id,
            'ledgerable_type' => get_class($payment),
            'issued_at' => $payment->paid_at,
            'entry_type' => 'total',
            'debit' => $payment->amount,
        ]);

        $accounts_receivable_id = Account::code('120')->value('id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $accounts_receivable_id,
            'ledgerable_id' => $payment->id,
            'ledgerable_type' => get_class($payment),
            'issued_at' => $payment->paid_at,
            'entry_type' => 'item',
            'credit' => $payment->amount,
        ]);
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $payment
     * @return void
     */
    public function deleted(Model $payment)
    {
        Ledger::record($payment->id, get_class($payment))->delete();
    }
}