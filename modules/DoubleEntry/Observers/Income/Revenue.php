<?php

namespace Modules\DoubleEntry\Observers\Income;

use App\Models\Income\Revenue as Model;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\AccountBank;
use Modules\DoubleEntry\Models\Ledger;

class Revenue
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $revenue
     * @return void
     */
    public function created(Model $revenue)
    {
        $account_id = AccountBank::where('bank_id', $revenue->account_id)->value('account_id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $account_id,
            'ledgerable_id' => $revenue->id,
            'ledgerable_type' => get_class($revenue),
            'issued_at' => $revenue->paid_at,
            'entry_type' => 'total',
            'debit' => $revenue->amount,
        ]);

        $accounts_receivable_id = Account::code('120')->value('id');

        $ledger = Ledger::create([
            'company_id' => session('company_id'),
            'account_id' => $accounts_receivable_id,
            'ledgerable_id' => $revenue->id,
            'ledgerable_type' => get_class($revenue),
            'issued_at' => $revenue->paid_at,
            'entry_type' => 'item',
            'credit' => $revenue->amount,
        ]);
    }

    /**
     * Listen to the created event.
     *
     * @param  Model  $revenue
     * @return void
     */
    public function updated(Model $revenue)
    {
        $ledger = Ledger::record($revenue->id, get_class($revenue))->where('entry_type', 'total')->first();

        $account_id = AccountBank::where('bank_id', $revenue->account_id)->value('account_id');

        $ledger->update([
            'company_id' => session('company_id'),
            'account_id' => $account_id,
            'ledgerable_id' => $revenue->id,
            'ledgerable_type' => get_class($revenue),
            'issued_at' => $revenue->paid_at,
            'entry_type' => 'total',
            'debit' => $revenue->amount,
        ]);

        $ledger = Ledger::record($revenue->id, get_class($revenue))->where('entry_type', 'item')->first();

        $accounts_receivable_id = Account::code('120')->value('id');

        $ledger->update([
            'company_id' => session('company_id'),
            'account_id' => $accounts_receivable_id,
            'ledgerable_id' => $revenue->id,
            'ledgerable_type' => get_class($revenue),
            'issued_at' => $revenue->paid_at,
            'entry_type' => 'item',
            'credit' => $revenue->amount,
        ]);
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $revenue
     * @return void
     */
    public function deleted(Model $revenue)
    {
        Ledger::record($revenue->id, get_class($revenue))->delete();
    }
}