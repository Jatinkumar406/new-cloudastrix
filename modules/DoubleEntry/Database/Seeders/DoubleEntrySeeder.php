<?php

namespace Modules\DoubleEntry\Database\Seeders;

use Illuminate\Database\Seeder;

class DoubleEntrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(\Modules\DoubleEntry\Database\Seeders\Accounts::class);
        $this->call(\Modules\DoubleEntry\Database\Seeders\Classes::class);
        $this->call(\Modules\DoubleEntry\Database\Seeders\Types::class);
    }
}
