<?php

namespace Modules\DoubleEntry\Database\Seeders;

use App\Models\Model;
use Modules\DoubleEntry\Models\DEClass;
use Illuminate\Database\Seeder;

class Classes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $rows = [
            [
                'name' => 'Assets',
            ],
            [
                'name' => 'Liabilities',
            ],
            [
                'name' => 'Expenses',
            ],
            [
                'name' => 'Income',
            ],
            [
                'name' => 'Equity',
            ],
        ];

        foreach ($rows as $row) {
            DEClass::create($row);
        }
    }
}
