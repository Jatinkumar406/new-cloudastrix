<?php

namespace Modules\DoubleEntry\Database\Seeders;

use App\Models\Model;
use Modules\DoubleEntry\Models\Type;
use Illuminate\Database\Seeder;

class Types extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $rows = [
            [
                'name' => 'Current Asset',
                'class_id' => '1',
            ],
            [
                'name' => 'Fixed Asset',
                'class_id' => '1',
            ],
            [
                'name' => 'Inventory',
                'class_id' => '1',
            ],
            [
                'name' => 'Non-current Asset',
                'class_id' => '1',
            ],
            [
                'name' => 'Prepayment',
                'class_id' => '1',
            ],
            [
                'name' => 'Bank & Cash',
                'class_id' => '1',
            ],
            [
                'name' => 'Current Liability',
                'class_id' => '2',
            ],
            [
                'name' => 'Liability',
                'class_id' => '2',
            ],
            [
                'name' => 'Non-current Liability',
                'class_id' => '2',
            ],
            [
                'name' => 'Depreciation',
                'class_id' => '3',
            ],
            [
                'name' => 'Direct Costs',
                'class_id' => '3',
            ],
            [
                'name' => 'Expense',
                'class_id' => '3',
            ],
            [
                'name' => 'Revenue',
                'class_id' => '4',
            ],
            [
                'name' => 'Sales',
                'class_id' => '4',
            ],
            [
                'name' => 'Other Income',
                'class_id' => '4',
            ],
            [
                'name' => 'Equity',
                'class_id' => '5',
            ],
        ];

        foreach ($rows as $row) {
            Type::create($row);
        }
    }
}
