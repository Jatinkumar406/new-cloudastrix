<?php

namespace Modules\DoubleEntry\Database\Seeders;

use App\Models\Model;
use Modules\DoubleEntry\Models\Account;
use Illuminate\Database\Seeder;

class Accounts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $company_id = 1;

        $rows = [
            [
                'company_id' => $company_id,
                'type_id' => '1',
                'code' => '120',
                'name' => 'Accounts Receivable',
                'system' => '1',
            ],
            [
                'company_id' => $company_id,
                'type_id' => '3',
                'code' => '140',
                'name' => 'Inventory',
                'system' => '1',
            ],
            [
                'company_id' => $company_id,
                'type_id' => '2',
                'code' => '150',
                'name' => 'Office Equipment',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '2',
                'code' => '151',
                'name' => 'Less Accumulated Depreciation on Office Equipment',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '2',
                'code' => '160',
                'name' => 'Computer Equipment',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '2',
                'code' => '161',
                'name' => 'Less Accumulated Depreciation on Computer Equipment',
                'system' => '1',
            ],
			/*[
                'company_id' => $company_id,
                'type_id' => '6',
                'code' => '090',
                'name' => 'Petty Cash',
                'system' => '1',
            ],*/
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '200',
                'name' => 'Accounts Payable',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '205',
                'name' => 'Accruals',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '210',
                'name' => 'Unpaid Expense Claims',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '215',
                'name' => 'Wages Payable',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '216',
                'name' => 'Wages Payable - Payroll',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '220',
                'name' => 'Sales Tax',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '230',
                'name' => 'Employee Tax Payable',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '235',
                'name' => 'Employee Benefits Payable',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '236',
                'name' => 'Employee Deductions payable',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '240',
                'name' => 'Income Tax Payable',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '250',
                'name' => 'Suspense',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '255',
                'name' => 'Historical Adjustments',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '260',
                'name' => 'Rounding',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '835',
                'name' => 'Revenue Received in Advance',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '7',
                'code' => '835',
                'name' => 'Clearing Account',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '9',
                'code' => '290',
                'name' => 'Loan',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '11',
                'code' => '500',
                'name' => 'Costs of Goods Sold',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '600',
                'name' => 'Advertising',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '605',
                'name' => 'Bank Service Charges',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '610',
                'name' => 'Janitorial Expenses',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '615',
                'name' => 'Consulting & Accounting',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '620',
                'name' => 'Entertainment',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '624',
                'name' => 'Postage & Delivary',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '628',
                'name' => 'General Expenses',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '632',
                'name' => 'Insurance',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '636',
                'name' => 'Legal Expenses',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '640',
                'name' => 'Utilities',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '644',
                'name' => 'Automobile Expenses',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '648',
                'name' => 'Office Expenses',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '652',
                'name' => 'Printing & Stationary',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '656',
                'name' => 'Rent',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '660',
                'name' => 'Repairs & Maintenance',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '664',
                'name' => 'Wages & Salaries',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '668',
                'name' => 'Payroll Tax Expense',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '672',
                'name' => 'Dues & Subscriptions',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '676',
                'name' => 'Telephone & Internet',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '680',
                'name' => 'Travel',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '684',
                'name' => 'Bad Debts',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '10',
                'code' => '700',
                'name' => 'Depreciation',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '710',
                'name' => 'Income Tax Expense',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '715',
                'name' => 'Employee Benefits Expense',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '800',
                'name' => 'Interest Expense',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '810',
                'name' => 'Bank Revaluations',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '815',
                'name' => 'Unrealized Currency Gains',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '820',
                'name' => 'Realized Currency Gains',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '12',
                'code' => '825',
                'name' => 'Sales Discount',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '13',
                'code' => '400',
                'name' => 'Sales',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '13',
                'code' => '460',
                'name' => 'Interest Income',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '13',
                'code' => '470',
                'name' => 'Other Revenue',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '13',
                'code' => '475',
                'name' => 'Purchase Discount',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '16',
                'code' => '300',
                'name' => 'Owners Contribution',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '16',
                'code' => '310',
                'name' => 'Owners Draw',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '16',
                'code' => '320',
                'name' => 'Retained Earnings',
                'system' => '1',
            ],
			[
                'company_id' => $company_id,
                'type_id' => '16',
                'code' => '330',
                'name' => 'Common Stock',
                'system' => '1',
            ],
			/*[
                'company_id' => $company_id,
                'type_id' => '6',
                'code' => '092',
                'name' => 'Savings Account',
                'system' => '1',
            ],*/
        ];

        foreach ($rows as $row) {
            Account::create($row);
        }
    }
}
