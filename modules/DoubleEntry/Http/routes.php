<?php

Route::group(['middleware' => 'admin', 'prefix' => 'double-entry', 'namespace' => 'Modules\DoubleEntry\Http\Controllers'], function() {
    Route::resource('chart-of-accounts', 'ChartOfAccounts');
    Route::resource('journal-entry', 'JournalEntry');
    Route::resource('general-ledger', 'GeneralLedger');
    Route::resource('balance-sheet', 'BalanceSheet');
    Route::resource('trial-balance', 'TrialBalance');
});
