<?php

namespace Modules\DoubleEntry\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\DoubleEntry\Models\Journal;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\Type;
use Modules\DoubleEntry\Http\Requests\Journal as Request;

class JournalEntry extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $limit = request('limit', setting('general.list_limit', '25'));

        $journals = Journal::paginate($limit);

        return view('doubleentry::double-entry.journal-entry.index', compact('journals'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return redirect('double-entry/journal-entry');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $accounts = [];

        $types = Type::pluck('name', 'id')->toArray();
        $all_accounts = Account::with(['type'])->get();

        foreach ($all_accounts as $account) {
            if (!isset($types[$account->type_id])) {
                continue;
            }

            $accounts[$types[$account->type_id]][$account->id] = $account->code . ' - ' . $account->name;
        }

        ksort($accounts);

        return view('doubleentry::double-entry.journal-entry.create', compact('accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $input['amount'] = $input['credit_amount'];

        $journal = Journal::create($input);

        $journal->ledger()->create([
            'company_id' => session('company_id'),
            'account_id' => $input['credit_account_id'],
            'issued_at' => $journal->paid_at,
            'entry_type' => 'item',
            'credit' => $input['credit_amount'],
        ]);

        $journal->ledger()->create([
            'company_id' => session('company_id'),
            'account_id' => $input['debit_account_id'],
            'issued_at' => $journal->paid_at,
            'entry_type' => 'item',
            'debit' => $input['debit_amount'],
        ]);

        $message = trans('messages.success.added', ['type' => trans('double-entry::general.journal_entry')]);

        flash($message)->success();

        return redirect('double-entry/journal-entry');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Journal  $journal_entry
     *
     * @return Response
     */
    public function edit(Journal $journal_entry)
    {
        $journal = $journal_entry;

        $accounts = [];

        $types = Type::pluck('name', 'id')->toArray();
        $all_accounts = Account::with(['type'])->get();

        foreach ($all_accounts as $account) {
            if (!isset($types[$account->type_id])) {
                continue;
            }

            $accounts[$types[$account->type_id]][$account->id] = $account->code . ' - ' . $account->name;
        }

        ksort($accounts);

        foreach ($journal->ledgers as $ledger) {
            if (!empty($ledger->debit)) {
                $journal->debit_account_id = $ledger->account_id;
                $journal->debit_amount = $ledger->debit;
            } else {
                $journal->credit_account_id = $ledger->account_id;
                $journal->credit_amount = $ledger->credit;
            }
        }

        return view('doubleentry::double-entry.journal-entry.edit', compact('journal', 'accounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Journal  $journal_entry
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Journal $journal_entry, Request $request)
    {
        $input = $request->input();
        $input['amount'] = $input['credit_amount'];

        $journal_entry->update($input);

        $journal_entry->ledger()->update([
            'company_id' => session('company_id'),
            'account_id' => $input['credit_account_id'],
            'issued_at' => $journal_entry->paid_at,
            'entry_type' => 'item',
            'credit' => $input['credit_amount'],
        ]);

        $journal_entry->ledger()->update([
            'company_id' => session('company_id'),
            'account_id' => $input['debit_account_id'],
            'issued_at' => $journal_entry->paid_at,
            'entry_type' => 'item',
            'debit' => $input['debit_amount'],
        ]);

        $message = trans('messages.success.updated', ['type' => trans('double-entry::general.journal_entry')]);

        flash($message)->success();

        return redirect('double-entry/journal-entry');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Journal  $journal_entry
     *
     * @return Response
     */
    public function destroy(Journal $journal_entry)
    {
        $journal_entry->delete();
        $journal_entry->ledgers()->delete();

        $message = trans('messages.success.deleted', ['type' => trans('double-entry::general.journal_entry')]);

        flash($message)->success();
    }
}
