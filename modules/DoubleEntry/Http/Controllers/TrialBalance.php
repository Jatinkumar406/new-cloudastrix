<?php

namespace Modules\DoubleEntry\Http\Controllers;

use App\Http\Controllers\Controller;
use Date;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\DEClass;
use Modules\DoubleEntry\Models\Type;

class TrialBalance extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Get year
        $year = request('year');
        if (empty($year)) {
            $year = Date::now()->year;
        }

        $classes = DEClass::with('accounts')->get();

        // Check if it's a print or normal request
        if (request('print')) {
            $template = 'print';
        } else {
            $template = 'index';
        }

        $accounts = [];

        foreach ($classes as $class) {
            $class->debit_total = $class->credit_total = 0;

            foreach ($class->accounts as $item) {
                $i = new \stdClass();
                $i->name = $item->name;
                $i->debit_total = $item->debit_total;
                $i->credit_total = $item->credit_total;

                $class->debit_total += $i->debit_total;
                $class->credit_total += $i->credit_total;

                $accounts[$class->id][] = $i;
            }
        }

        return view('doubleentry::double-entry.trial-balance.' . $template, compact('classes', 'accounts'));
    }
}
