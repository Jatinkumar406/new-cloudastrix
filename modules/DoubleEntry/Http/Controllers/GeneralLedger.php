<?php

namespace Modules\DoubleEntry\Http\Controllers;

use App\Http\Controllers\Controller;
use Date;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\DEClass;
use Modules\DoubleEntry\Models\Ledger;

class GeneralLedger extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Get year
        $year = request('year');
        if (empty($year)) {
            $year = Date::now()->year;
        }

        $limit = request('limit', setting('general.list_limit', '25'));

        $ledgers = Ledger::with(['account'])->orderBy('issued_at', 'desc')->paginate($limit);

        // Check if it's a print or normal request
        if (request('print')) {
            $template = 'print';
        } else {
            $template = 'index';
        }

        return view('doubleentry::double-entry.general-ledger.' . $template, compact('ledgers'));
    }
}
