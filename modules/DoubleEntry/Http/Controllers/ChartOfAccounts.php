<?php

namespace Modules\DoubleEntry\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\DEClass;
use Modules\DoubleEntry\Models\Type;
use Modules\DoubleEntry\Http\Requests\Account as Request;

class ChartOfAccounts extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $classes = DEClass::with('accounts')->get();

        return view('doubleentry::double-entry.chart-of-accounts.index', compact('classes'));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return redirect('double-entry/chart-of-accounts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $types = [];

        $classes = DEClass::pluck('name', 'id')->toArray();
        $all_types = Type::all();

        foreach ($all_types as $type) {
            if (!isset($classes[$type->class_id])) {
                continue;
            }

            $types[$classes[$type->class_id]][$type->id] = $type->name;
        }

        ksort($types);

        return view('doubleentry::double-entry.chart-of-accounts.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        Account::create($request->all());

        $message = trans('messages.success.added', ['type' => trans_choice('general.accounts', 1)]);

        flash($message)->success();

        return redirect('double-entry/chart-of-accounts');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Account  $chart_of_account
     *
     * @return Response
     */
    public function edit(Account $chart_of_account)
    {
        $account = $chart_of_account;

        $types = [];

        $classes = DEClass::pluck('name', 'id')->toArray();
        $all_types = Type::all();

        foreach ($all_types as $type) {
            if (!isset($classes[$type->class_id])) {
                continue;
            }

            $types[$classes[$type->class_id]][$type->id] = $type->name;
        }

        ksort($types);

        return view('doubleentry::double-entry.chart-of-accounts.edit', compact('account', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Account  $chart_of_account
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Account $chart_of_account, Request $request)
    {
        /*$relationships = $this->countRelationships($tax, [
            'items' => 'items',
            'invoice_items' => 'invoices',
            'bill_items' => 'bills',
        ]);*/

        if (empty($relationships) || $request['enabled']) {
            $chart_of_account->update($request->all());

            $message = trans('messages.success.updated', ['type' => trans_choice('general.accounts', 1)]);

            flash($message)->success();

            return redirect('double-entry/chart-of-accounts');
        } else {
            $message = trans('messages.warning.disabled', ['name' => $chart_of_account->name, 'text' => implode(', ', $relationships)]);

            flash($message)->warning();

            return redirect('double-entry/chart-of-accounts/' . $chart_of_account->id . '/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Account  $chart_of_account
     *
     * @return Response
     */
    public function destroy(Account $chart_of_account)
    {
        /*$relationships = $this->countRelationships($account, [
            'items' => 'items',
            'invoice_items' => 'invoices',
            'bill_items' => 'bills',
        ]);*/

        if (empty($relationships)) {
            $chart_of_account->delete();

            $message = trans('messages.success.deleted', ['type' => trans_choice('general.accounts', 1)]);

            flash($message)->success();
        } else {
            $message = trans('messages.warning.deleted', ['name' => $chart_of_account->name, 'text' => implode(', ', $relationships)]);

            flash($message)->warning();
        }

        return redirect('double-entry/chart-of-accounts');
    }
}
