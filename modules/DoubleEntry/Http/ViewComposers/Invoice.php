<?php

namespace Modules\DoubleEntry\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Income\InvoiceItem;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\Ledger;
use Modules\DoubleEntry\Models\Type;
use App\Models\Module\Module;

class Invoice
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $list = [];

        $types = Type::where('class_id', 4)->pluck('name', 'id')->toArray();

        $accounts = Account::with(['type'])->get();

        foreach ($accounts as $account) {
            if (!isset($types[$account->type_id])) {
                continue;
            }

            $list[$types[$account->type_id]][$account->id] = $account->code . ' - ' . $account->name;
        }

        ksort($list);

        $view->with(['de_accounts' => $list]);

        $url = explode('/', request()->url());
        $action = array_pop($url);

        if ($action == 'edit') {
            $invoice_item_accounts = [];
            $invoice_id = end($url);

            $items = InvoiceItem::where('invoice_id', $invoice_id)->pluck('id');

            foreach ($items as $item_id) {
                $account_id = Ledger::record($item_id, 'App\Models\Income\InvoiceItem')->pluck('account_id');

                if (empty($account_id)) {
                    continue;
                }

                $invoice_item_accounts[$item_id] = $account_id;
            }

            $view->with(['invoice_item_accounts' => $invoice_item_accounts]);
        }

        // Override the whole file
        $path = $this->isProject($action);

        $view->setPath(view($path)->getPath());
    }

    protected function isProject($action)
    {
        $path = 'doubleentry::incomes.invoices.' . $action;

        $module = Module::where(['alias' => 'project', 'status' => '1'])->first();

        if ($module) {
            $path = 'doubleentry::compatibility.project.invoices.' . $action;
        }

        return $path;
    }
}
