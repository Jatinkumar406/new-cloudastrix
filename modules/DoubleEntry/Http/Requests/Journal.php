<?php

namespace Modules\DoubleEntry\Http\Requests;

use App\Http\Requests\Request;

class Journal extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paid_at' => 'required|date',
            'credit_amount' => 'required',
            'credit_account_id' => 'required|integer',
            'debit_amount' => 'required',
            'debit_account_id' => 'required|integer',
            'description' => 'required',
        ];
    }
}
