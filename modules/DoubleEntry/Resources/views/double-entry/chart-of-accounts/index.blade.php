@extends('layouts.admin')

@section('title', trans('doubleentry::general.chart_of_accounts'))

@section('new_button')
<span class="new-button"><a href="{{ url('double-entry/chart-of-accounts/create') }}" class="btn btn-success btn-sm"><span class="fa fa-plus"></span> &nbsp;Add New</a></span>
@endsection

@section('content')
@foreach($classes as $class)
<!-- Default box -->
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $class->name }}</h3>
    </div>
    <div class="box-body">
        <div class="table table-responsive">
            <table class="table table-striped table-hover" id="tbl-taxes">
                <thead>
                    <tr>
                        <th class="col-md-1">{{ trans('general.code') }}</th>
                        <th class="col-md-6">{{ trans('general.name') }}</th>
                        <th class="col-md-3">{{ trans_choice('general.types', 1) }}</th>
                        <th class="col-md-2 text-center">{{ trans('general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($class->accounts as $item)
                    <tr>
                        <td>{{ $item->code }}</td>
                        <td><a href="{{ url('double-entry/chart-of-accounts/' . $item->id . '/edit') }}">{{ $item->name }}</a></td>
                        <td>{{ $item->type->name }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-toggle-position="left" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ url('double-entry/chart-of-accounts/' . $item->id . '/edit') }}">{{ trans('general.edit') }}</a></li>
                                    <li>{!! Form::deleteLink($item, 'double-entry/chart-of-accounts', 'tax_rates') !!}</li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
</div>
@endforeach
<!-- /.box -->
@endsection
