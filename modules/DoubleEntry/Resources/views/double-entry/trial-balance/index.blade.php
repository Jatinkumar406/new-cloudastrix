@extends('layouts.admin')

@section('title', trans('doubleentry::general.trial_balance'))

@section('new_button')
    <span class="new-button"><a href="{{ url('double-entry/trial-balance') }}?print=1&year={{ request('year', $this_year) }}" target="_blank" class="btn btn-success btn-sm"><span class="fa fa-print"></span> &nbsp;{{ trans('general.print') }}</a></span>
@endsection

@section('content')
    <!-- Default box -->
    <div class="box box-success">
        @include('doubleentry::double-entry.trial-balance.body')
    </div>
    <!-- /.box -->
@endsection
