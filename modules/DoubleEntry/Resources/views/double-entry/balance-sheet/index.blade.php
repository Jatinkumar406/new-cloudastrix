@extends('layouts.admin')

@section('title', trans('doubleentry::general.balance_sheet'))

@section('new_button')
    <span class="new-button"><a href="{{ url('double-entry/balance-sheet') }}?print=1&year={{ request('year', $this_year) }}" target="_blank" class="btn btn-success btn-sm"><span class="fa fa-print"></span> &nbsp;{{ trans('general.print') }}</a></span>
@endsection

@section('content')
    @include('doubleentry::double-entry.balance-sheet.body')
@endsection
