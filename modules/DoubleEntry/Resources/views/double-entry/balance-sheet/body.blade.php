@foreach($classes as $class)
@if (!empty($class->debit_total) || !empty($class->credit_total))
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $class->name }}</h3>
        </div>
        <div class="box-body">
            <div class="table table-responsive">
                <table class="table table-striped table-hover" id="tbl-taxes">
                    <thead>
                        <tr>
                            <th class="col-md-5">{{ trans('general.name') }}</th>
                            <th class="col-md-7 text-right">{{ trans_choice('general.totals', 1) }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts[$class->id] as $item)
                        @if (!empty($item->debit_total) || !empty($item->credit_total))
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td class="text-right">@money(($item->debit_total - $item->credit_total), setting('general.default_currency'), true)</td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@endif
@endforeach
<!-- /.box -->
