@extends('layouts.admin')

@section('title', trans('general.title.new', ['type' => trans('doubleentry::general.journal_entry')]))

@section('content')
    <div class="row">
        {!! Form::model($journal, [
            'method' => 'PATCH',
            'url' => ['double-entry/journal-entry', $journal->id],
            'role' => 'form'
        ]) !!}

        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('doubleentry::general.debit') }}</h3>
                </div>

                <div class="box-body">
                    {{ Form::selectGroup('debit_account_id', trans_choice('general.accounts', 1), 'book', $accounts) }}

                    {{ Form::textGroup('debit_amount', trans('general.amount'), 'money', ['required' => 'required']) }}
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('doubleentry::general.credit') }}</h3>
                </div>

                <div class="box-body">
                    {{ Form::selectGroup('credit_account_id', trans_choice('general.accounts', 1), 'book', $accounts) }}

                    {{ Form::textGroup('credit_amount', trans('general.amount'), 'money', ['required' => 'required']) }}
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body">
                    {{ Form::textGroup('paid_at', trans('general.date'), 'calendar',['id' => 'paid_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy-mm-dd\'', 'data-mask' => ''], Date::now()->toDateString()) }}

                    {{ Form::textGroup('reference', trans('general.reference'), 'file-text-o', []) }}

                    {{ Form::textareaGroup('description', trans('general.description'), null, ['rows' => '3', 'required' => 'required']) }}
                </div>

                <div class="box-footer">
                    {{ Form::saveButtons('double-entry/journal-entry') }}
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
@endsection

@push('js')
<script src="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/locales/bootstrap-datepicker.' . language()->getShortCode() . '.js') }}"></script>
@endpush

@push('css')
<link rel="stylesheet" href="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/datepicker3.css') }}">
@endpush

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        //Date picker
        $('#paid_at').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: '{{ language()->getShortCode() }}'
        });

        $("#debit_account_id").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.accounts', 1)]) }}"
        });

        $("#credit_account_id").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.accounts', 1)]) }}"
        });
    });
</script>
@endpush
