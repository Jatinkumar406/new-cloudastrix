@extends('layouts.print')

@section('title', trans('doubleentry::general.general_ledger'))

@section('content')
    <div class="box-header">
        <h2>{{ trans('doubleentry::general.general_ledger') }}</h2>
        <div class="text-muted">
            {{ setting('general.company_name') }}
            <br/>
            {{ Date::parse(request('year') . '-1-1')->format($date_format) }} - {{ Date::parse(request('year') . '-12-31')->format($date_format) }}
        </div>
    </div>
    @include('doubleentry::double-entry.general-ledger.body')
@endsection