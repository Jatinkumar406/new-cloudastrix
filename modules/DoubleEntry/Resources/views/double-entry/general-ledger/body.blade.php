<div class="box-body">
    <div class="table table-responsive">
        <table class="table table-striped table-hover" id="tbl-taxes">
            <thead>
                <tr>
                    <th class="col-md-2">{{ trans('general.date') }}</th>
                    <th class="col-md-4">{{ trans_choice('general.accounts', 1) }}</th>
                    <th class="col-md-2 text-right amount-space">{{ trans('general.amount') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($ledgers as $item)
                <tr>
                    <td>{{ Date::parse($item->issued_at)->format($date_format) }}</td>
                    <td>{{ $item->account->name }}</td>
                    @if ($item->debit)
                    <td class="text-right amount-space">@money($item->debit, setting('general.default_currency'), true)</td>
                    @else
                    <td class="text-right amount-space">@money($item->credit, setting('general.default_currency'), true)</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- /.box-body -->
