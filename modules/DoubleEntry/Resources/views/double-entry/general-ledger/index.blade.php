@extends('layouts.admin')

@section('title', trans('doubleentry::general.general_ledger'))

@section('new_button')
    <span class="new-button"><a href="{{ url('double-entry/general-ledger') }}?print=1&year={{ request('year', $this_year) }}" target="_blank" class="btn btn-success btn-sm"><span class="fa fa-print"></span> &nbsp;{{ trans('general.print') }}</a></span>
@endsection

@section('content')<!-- Default box -->
    <div class="box box-success">
        @include('doubleentry::double-entry.general-ledger.body')

        <div class="box-footer">
            @include('partials.admin.pagination', ['items' => $ledgers, 'type' => 'transactions'])
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
@endsection
