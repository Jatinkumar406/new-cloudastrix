<?php

namespace Modules\DoubleEntry\Models;

use App\Models\Model;

class AccountBank extends Model
{

    protected $table = 'double_entry_account_bank';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'account_id', 'bank_id'];

    public function account()
    {
        return $this->belongsTo('Modules\DoubleEntry\Models\Account');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Banking\Account');
    }
}
