<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    /*protected $commands = [
        Commands\CompanySeed::class,
        Commands\BillReminder::class,
        Commands\Install::class,
        Commands\InvoiceReminder::class,
        Commands\ModuleDisable::class,
        Commands\ModuleEnable::class,
        Commands\ModuleInstall::class,
        Commands\RecurringCheck::class,
    ];*/
    /*shoaib Code for cron work*/
    protected $commands = [
        'App\Console\Commands\CompanySeed',
        'App\Console\Commands\BillReminder',
        'App\Console\Commands\Install',
        'App\Console\Commands\InvoiceReminder',        
        'App\Console\Commands\ModuleDisable',
        'App\Console\Commands\ModuleEnable',
        'App\Console\Commands\ModuleInstall',
        'App\Console\Commands\RecurringCheck',
    ];
     /*dev Shoaib modification end*/
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Not installed yet
        if (!env('APP_INSTALLED')) {
            return;
        }

        $schedule->command('reminder:invoice')->dailyAt(setting('general.schedule_time', '09:00'));
        $schedule->command('reminder:bill')->dailyAt(setting('general.schedule_time', '09:00'));
        $schedule->command('recurring:check')->dailyAt(setting('general.schedule_time', '09:00'));
       
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
