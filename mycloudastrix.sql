-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2018 at 08:24 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mycloudastrix`
--

-- --------------------------------------------------------

--
-- Table structure for table `fss_accounts`
--

CREATE TABLE `fss_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` double(15,4) NOT NULL DEFAULT '0.0000',
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_address` text COLLATE utf8mb4_unicode_ci,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_accounts`
--

INSERT INTO `fss_accounts` (`id`, `company_id`, `name`, `number`, `currency_code`, `opening_balance`, `bank_name`, `bank_phone`, `bank_address`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Cash', '1', 'USD', 0.0000, 'Cash', NULL, NULL, 1, '2018-08-28 11:04:56', '2018-08-28 11:04:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_bills`
--

CREATE TABLE `fss_bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `bill_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_status_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billed_at` datetime NOT NULL,
  `due_at` datetime NOT NULL,
  `amount` double(15,4) NOT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` double(15,8) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `vendor_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_tax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_address` text COLLATE utf8mb4_unicode_ci,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_bill_histories`
--

CREATE TABLE `fss_bill_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `status_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_bill_items`
--

CREATE TABLE `fss_bill_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` double(7,2) NOT NULL,
  `price` double(15,4) NOT NULL,
  `total` double(15,4) NOT NULL,
  `tax` double(15,4) NOT NULL DEFAULT '0.0000',
  `tax_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_bill_payments`
--

CREATE TABLE `fss_bill_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `paid_at` datetime NOT NULL,
  `amount` double(15,4) NOT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` double(15,8) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_bill_statuses`
--

CREATE TABLE `fss_bill_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_bill_statuses`
--

INSERT INTO `fss_bill_statuses` (`id`, `company_id`, `name`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Draft', 'draft', '2018-08-28 11:04:56', '2018-08-28 11:04:56', NULL),
(2, 1, 'Received', 'received', '2018-08-28 11:04:56', '2018-08-28 11:04:56', NULL),
(3, 1, 'Partial', 'partial', '2018-08-28 11:04:56', '2018-08-28 11:04:56', NULL),
(4, 1, 'Paid', 'paid', '2018-08-28 11:04:56', '2018-08-28 11:04:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_bill_totals`
--

CREATE TABLE `fss_bill_totals` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(15,4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_categories`
--

CREATE TABLE `fss_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_categories`
--

INSERT INTO `fss_categories` (`id`, `company_id`, `name`, `type`, `color`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Transfer', 'other', '#605ca8', 1, '2018-08-28 11:04:56', '2018-08-28 11:04:56', NULL),
(2, 1, 'Deposit', 'income', '#f39c12', 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(3, 1, 'Sales', 'income', '#6da252', 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(4, 1, 'Other', 'expense', '#d2d6de', 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(5, 1, 'General', 'item', '#00c0ef', 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_companies`
--

CREATE TABLE `fss_companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `domain` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_companies`
--

INSERT INTO `fss_companies` (`id`, `domain`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', 1, '2018-08-28 11:04:56', '2018-08-28 11:04:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_currencies`
--

CREATE TABLE `fss_currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double(15,8) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `precision` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `symbol_first` int(11) NOT NULL DEFAULT '1',
  `decimal_mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thousands_separator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_currencies`
--

INSERT INTO `fss_currencies` (`id`, `company_id`, `name`, `code`, `rate`, `enabled`, `created_at`, `updated_at`, `deleted_at`, `precision`, `symbol`, `symbol_first`, `decimal_mark`, `thousands_separator`) VALUES
(1, 1, 'US Dollar', 'USD', 1.00000000, 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL, '2', '$', 1, '.', ','),
(2, 1, 'Euro', 'EUR', 1.25000000, 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL, '2', '€', 1, ',', '.'),
(3, 1, 'British Pound', 'GBP', 1.60000000, 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL, '2', '£', 1, '.', ','),
(4, 1, 'Turkish Lira', 'TRY', 0.80000000, 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL, '2', '₺', 1, ',', '.');

-- --------------------------------------------------------

--
-- Table structure for table `fss_customers`
--

CREATE TABLE `fss_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_double_entry_accounts`
--

CREATE TABLE `fss_double_entry_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_double_entry_accounts`
--

INSERT INTO `fss_double_entry_accounts` (`id`, `company_id`, `type_id`, `code`, `name`, `description`, `system`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '120', 'Accounts Receivable', NULL, 1, 1, '2018-08-28 06:27:58', '2018-08-28 06:27:58', NULL),
(2, 1, 3, '140', 'Inventory', NULL, 1, 1, '2018-08-28 06:27:58', '2018-08-28 06:27:58', NULL),
(3, 1, 2, '150', 'Office Equipment', NULL, 1, 1, '2018-08-28 06:27:58', '2018-08-28 06:27:58', NULL),
(4, 1, 2, '151', 'Less Accumulated Depreciation on Office Equipment', NULL, 1, 1, '2018-08-28 06:27:58', '2018-08-28 06:27:58', NULL),
(5, 1, 2, '160', 'Computer Equipment', NULL, 1, 1, '2018-08-28 06:27:58', '2018-08-28 06:27:58', NULL),
(6, 1, 2, '161', 'Less Accumulated Depreciation on Computer Equipment', NULL, 1, 1, '2018-08-28 06:27:58', '2018-08-28 06:27:58', NULL),
(7, 1, 7, '200', 'Accounts Payable', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(8, 1, 7, '205', 'Accruals', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(9, 1, 7, '210', 'Unpaid Expense Claims', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(10, 1, 7, '215', 'Wages Payable', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(11, 1, 7, '216', 'Wages Payable - Payroll', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(12, 1, 7, '220', 'Sales Tax', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(13, 1, 7, '230', 'Employee Tax Payable', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(14, 1, 7, '235', 'Employee Benefits Payable', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(15, 1, 7, '236', 'Employee Deductions payable', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(16, 1, 7, '240', 'Income Tax Payable', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(17, 1, 7, '250', 'Suspense', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(18, 1, 7, '255', 'Historical Adjustments', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(19, 1, 7, '260', 'Rounding', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(20, 1, 7, '835', 'Revenue Received in Advance', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(21, 1, 7, '835', 'Clearing Account', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(22, 1, 9, '290', 'Loan', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(23, 1, 11, '500', 'Costs of Goods Sold', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(24, 1, 12, '600', 'Advertising', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(25, 1, 12, '605', 'Bank Service Charges', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(26, 1, 12, '610', 'Janitorial Expenses', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(27, 1, 12, '615', 'Consulting & Accounting', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(28, 1, 12, '620', 'Entertainment', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(29, 1, 12, '624', 'Postage & Delivary', NULL, 1, 1, '2018-08-28 06:27:59', '2018-08-28 06:27:59', NULL),
(30, 1, 12, '628', 'General Expenses', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(31, 1, 12, '632', 'Insurance', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(32, 1, 12, '636', 'Legal Expenses', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(33, 1, 12, '640', 'Utilities', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(34, 1, 12, '644', 'Automobile Expenses', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(35, 1, 12, '648', 'Office Expenses', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(36, 1, 12, '652', 'Printing & Stationary', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(37, 1, 12, '656', 'Rent', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(38, 1, 12, '660', 'Repairs & Maintenance', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(39, 1, 12, '664', 'Wages & Salaries', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(40, 1, 12, '668', 'Payroll Tax Expense', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(41, 1, 12, '672', 'Dues & Subscriptions', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(42, 1, 12, '676', 'Telephone & Internet', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(43, 1, 12, '680', 'Travel', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(44, 1, 12, '684', 'Bad Debts', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(45, 1, 10, '700', 'Depreciation', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(46, 1, 12, '710', 'Income Tax Expense', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(47, 1, 12, '715', 'Employee Benefits Expense', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(48, 1, 12, '800', 'Interest Expense', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(49, 1, 12, '810', 'Bank Revaluations', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(50, 1, 12, '815', 'Unrealized Currency Gains', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(51, 1, 12, '820', 'Realized Currency Gains', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(52, 1, 12, '825', 'Sales Discount', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(53, 1, 13, '400', 'Sales', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(54, 1, 13, '460', 'Interest Income', NULL, 1, 1, '2018-08-28 06:28:00', '2018-08-28 06:28:00', NULL),
(55, 1, 13, '470', 'Other Revenue', NULL, 1, 1, '2018-08-28 06:28:01', '2018-08-28 06:28:01', NULL),
(56, 1, 13, '475', 'Purchase Discount', NULL, 1, 1, '2018-08-28 06:28:01', '2018-08-28 06:28:01', NULL),
(57, 1, 16, '300', 'Owners Contribution', NULL, 1, 1, '2018-08-28 06:28:01', '2018-08-28 06:28:01', NULL),
(58, 1, 16, '310', 'Owners Draw', NULL, 1, 1, '2018-08-28 06:28:01', '2018-08-28 06:28:01', NULL),
(59, 1, 16, '320', 'Retained Earnings', NULL, 1, 1, '2018-08-28 06:28:01', '2018-08-28 06:28:01', NULL),
(60, 1, 16, '330', 'Common Stock', NULL, 1, 1, '2018-08-28 06:28:01', '2018-08-28 06:28:01', NULL),
(61, 1, 6, '836', 'Cash', NULL, 0, 1, '2018-08-28 06:41:38', '2018-08-28 06:41:38', NULL),
(62, 1, 1, '120', 'Accounts Receivable', NULL, 1, 1, '2018-08-28 06:44:15', '2018-08-28 06:44:15', NULL),
(63, 1, 3, '140', 'Inventory', NULL, 1, 1, '2018-08-28 06:44:15', '2018-08-28 06:44:15', NULL),
(64, 1, 2, '150', 'Office Equipment', NULL, 1, 1, '2018-08-28 06:44:15', '2018-08-28 06:44:15', NULL),
(65, 1, 2, '151', 'Less Accumulated Depreciation on Office Equipment', NULL, 1, 1, '2018-08-28 06:44:15', '2018-08-28 06:44:15', NULL),
(66, 1, 2, '160', 'Computer Equipment', NULL, 1, 1, '2018-08-28 06:44:15', '2018-08-28 06:44:15', NULL),
(67, 1, 2, '161', 'Less Accumulated Depreciation on Computer Equipment', NULL, 1, 1, '2018-08-28 06:44:15', '2018-08-28 06:44:15', NULL),
(68, 1, 7, '200', 'Accounts Payable', NULL, 1, 1, '2018-08-28 06:44:15', '2018-08-28 06:44:15', NULL),
(69, 1, 7, '205', 'Accruals', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(70, 1, 7, '210', 'Unpaid Expense Claims', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(71, 1, 7, '215', 'Wages Payable', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(72, 1, 7, '216', 'Wages Payable - Payroll', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(73, 1, 7, '220', 'Sales Tax', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(74, 1, 7, '230', 'Employee Tax Payable', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(75, 1, 7, '235', 'Employee Benefits Payable', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(76, 1, 7, '236', 'Employee Deductions payable', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(77, 1, 7, '240', 'Income Tax Payable', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(78, 1, 7, '250', 'Suspense', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(79, 1, 7, '255', 'Historical Adjustments', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(80, 1, 7, '260', 'Rounding', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(81, 1, 7, '835', 'Revenue Received in Advance', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(82, 1, 7, '835', 'Clearing Account', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(83, 1, 9, '290', 'Loan', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(84, 1, 11, '500', 'Costs of Goods Sold', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(85, 1, 12, '600', 'Advertising', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(86, 1, 12, '605', 'Bank Service Charges', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(87, 1, 12, '610', 'Janitorial Expenses', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(88, 1, 12, '615', 'Consulting & Accounting', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(89, 1, 12, '620', 'Entertainment', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(90, 1, 12, '624', 'Postage & Delivary', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(91, 1, 12, '628', 'General Expenses', NULL, 1, 1, '2018-08-28 06:44:16', '2018-08-28 06:44:16', NULL),
(92, 1, 12, '632', 'Insurance', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(93, 1, 12, '636', 'Legal Expenses', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(94, 1, 12, '640', 'Utilities', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(95, 1, 12, '644', 'Automobile Expenses', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(96, 1, 12, '648', 'Office Expenses', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(97, 1, 12, '652', 'Printing & Stationary', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(98, 1, 12, '656', 'Rent', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(99, 1, 12, '660', 'Repairs & Maintenance', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(100, 1, 12, '664', 'Wages & Salaries', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(101, 1, 12, '668', 'Payroll Tax Expense', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(102, 1, 12, '672', 'Dues & Subscriptions', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(103, 1, 12, '676', 'Telephone & Internet', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(104, 1, 12, '680', 'Travel', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(105, 1, 12, '684', 'Bad Debts', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(106, 1, 10, '700', 'Depreciation', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(107, 1, 12, '710', 'Income Tax Expense', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(108, 1, 12, '715', 'Employee Benefits Expense', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(109, 1, 12, '800', 'Interest Expense', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(110, 1, 12, '810', 'Bank Revaluations', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(111, 1, 12, '815', 'Unrealized Currency Gains', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(112, 1, 12, '820', 'Realized Currency Gains', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(113, 1, 12, '825', 'Sales Discount', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(114, 1, 13, '400', 'Sales', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(115, 1, 13, '460', 'Interest Income', NULL, 1, 1, '2018-08-28 06:44:17', '2018-08-28 06:44:17', NULL),
(116, 1, 13, '470', 'Other Revenue', NULL, 1, 1, '2018-08-28 06:44:18', '2018-08-28 06:44:18', NULL),
(117, 1, 13, '475', 'Purchase Discount', NULL, 1, 1, '2018-08-28 06:44:18', '2018-08-28 06:44:18', NULL),
(118, 1, 16, '300', 'Owners Contribution', NULL, 1, 1, '2018-08-28 06:44:18', '2018-08-28 06:44:18', NULL),
(119, 1, 16, '310', 'Owners Draw', NULL, 1, 1, '2018-08-28 06:44:18', '2018-08-28 06:44:18', NULL),
(120, 1, 16, '320', 'Retained Earnings', NULL, 1, 1, '2018-08-28 06:44:18', '2018-08-28 06:44:18', NULL),
(121, 1, 16, '330', 'Common Stock', NULL, 1, 1, '2018-08-28 06:44:18', '2018-08-28 06:44:18', NULL),
(122, 1, 1, '120', 'Accounts Receivable', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(123, 1, 3, '140', 'Inventory', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(124, 1, 2, '150', 'Office Equipment', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(125, 1, 2, '151', 'Less Accumulated Depreciation on Office Equipment', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(126, 1, 2, '160', 'Computer Equipment', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(127, 1, 2, '161', 'Less Accumulated Depreciation on Computer Equipment', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(128, 1, 7, '200', 'Accounts Payable', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(129, 1, 7, '205', 'Accruals', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(130, 1, 7, '210', 'Unpaid Expense Claims', NULL, 1, 1, '2018-08-28 12:37:57', '2018-08-28 12:37:57', NULL),
(131, 1, 7, '215', 'Wages Payable', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(132, 1, 7, '216', 'Wages Payable - Payroll', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(133, 1, 7, '220', 'Sales Tax', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(134, 1, 7, '230', 'Employee Tax Payable', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(135, 1, 7, '235', 'Employee Benefits Payable', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(136, 1, 7, '236', 'Employee Deductions payable', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(137, 1, 7, '240', 'Income Tax Payable', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(138, 1, 7, '250', 'Suspense', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(139, 1, 7, '255', 'Historical Adjustments', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(140, 1, 7, '260', 'Rounding', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(141, 1, 7, '835', 'Revenue Received in Advance', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(142, 1, 7, '835', 'Clearing Account', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(143, 1, 9, '290', 'Loan', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(144, 1, 11, '500', 'Costs of Goods Sold', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(145, 1, 12, '600', 'Advertising', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(146, 1, 12, '605', 'Bank Service Charges', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(147, 1, 12, '610', 'Janitorial Expenses', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(148, 1, 12, '615', 'Consulting & Accounting', NULL, 1, 1, '2018-08-28 12:37:58', '2018-08-28 12:37:58', NULL),
(149, 1, 12, '620', 'Entertainment', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(150, 1, 12, '624', 'Postage & Delivary', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(151, 1, 12, '628', 'General Expenses', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(152, 1, 12, '632', 'Insurance', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(153, 1, 12, '636', 'Legal Expenses', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(154, 1, 12, '640', 'Utilities', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(155, 1, 12, '644', 'Automobile Expenses', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(156, 1, 12, '648', 'Office Expenses', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(157, 1, 12, '652', 'Printing & Stationary', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(158, 1, 12, '656', 'Rent', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(159, 1, 12, '660', 'Repairs & Maintenance', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(160, 1, 12, '664', 'Wages & Salaries', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(161, 1, 12, '668', 'Payroll Tax Expense', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(162, 1, 12, '672', 'Dues & Subscriptions', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(163, 1, 12, '676', 'Telephone & Internet', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(164, 1, 12, '680', 'Travel', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(165, 1, 12, '684', 'Bad Debts', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(166, 1, 10, '700', 'Depreciation', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(167, 1, 12, '710', 'Income Tax Expense', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(168, 1, 12, '715', 'Employee Benefits Expense', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(169, 1, 12, '800', 'Interest Expense', NULL, 1, 1, '2018-08-28 12:37:59', '2018-08-28 12:37:59', NULL),
(170, 1, 12, '810', 'Bank Revaluations', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(171, 1, 12, '815', 'Unrealized Currency Gains', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(172, 1, 12, '820', 'Realized Currency Gains', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(173, 1, 12, '825', 'Sales Discount', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(174, 1, 13, '400', 'Sales', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(175, 1, 13, '460', 'Interest Income', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(176, 1, 13, '470', 'Other Revenue', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(177, 1, 13, '475', 'Purchase Discount', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(178, 1, 16, '300', 'Owners Contribution', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(179, 1, 16, '310', 'Owners Draw', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(180, 1, 16, '320', 'Retained Earnings', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL),
(181, 1, 16, '330', 'Common Stock', NULL, 1, 1, '2018-08-28 12:38:00', '2018-08-28 12:38:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_double_entry_account_bank`
--

CREATE TABLE `fss_double_entry_account_bank` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_double_entry_classes`
--

CREATE TABLE `fss_double_entry_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_double_entry_classes`
--

INSERT INTO `fss_double_entry_classes` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Assets', '2018-08-28 06:13:09', '2018-08-28 06:13:09', NULL),
(2, 'Liabilities', '2018-08-28 06:13:09', '2018-08-28 06:13:09', NULL),
(3, 'Expenses', '2018-08-28 06:13:09', '2018-08-28 06:13:09', NULL),
(4, 'Income', '2018-08-28 06:13:09', '2018-08-28 06:13:09', NULL),
(5, 'Equity', '2018-08-28 06:13:09', '2018-08-28 06:13:09', NULL),
(6, 'Assets', '2018-08-28 12:36:44', '2018-08-28 12:36:44', NULL),
(7, 'Liabilities', '2018-08-28 12:36:44', '2018-08-28 12:36:44', NULL),
(8, 'Expenses', '2018-08-28 12:36:44', '2018-08-28 12:36:44', NULL),
(9, 'Income', '2018-08-28 12:36:44', '2018-08-28 12:36:44', NULL),
(10, 'Equity', '2018-08-28 12:36:44', '2018-08-28 12:36:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_double_entry_journals`
--

CREATE TABLE `fss_double_entry_journals` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `paid_at` date NOT NULL,
  `amount` double(15,4) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_double_entry_ledger`
--

CREATE TABLE `fss_double_entry_ledger` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `ledgerable_id` int(10) UNSIGNED NOT NULL,
  `ledgerable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `issued_at` date NOT NULL,
  `entry_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debit` double(15,4) DEFAULT NULL,
  `credit` double(15,4) DEFAULT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_double_entry_types`
--

CREATE TABLE `fss_double_entry_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_double_entry_types`
--

INSERT INTO `fss_double_entry_types` (`id`, `class_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Current Asset', '2018-08-28 06:13:23', '2018-08-28 06:13:23', NULL),
(2, 1, 'Fixed Asset', '2018-08-28 06:13:23', '2018-08-28 06:13:23', NULL),
(3, 1, 'Inventory', '2018-08-28 06:13:23', '2018-08-28 06:13:23', NULL),
(4, 1, 'Non-current Asset', '2018-08-28 06:13:23', '2018-08-28 06:13:23', NULL),
(5, 1, 'Prepayment', '2018-08-28 06:13:23', '2018-08-28 06:13:23', NULL),
(6, 1, 'Bank & Cash', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(7, 2, 'Current Liability', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(8, 2, 'Liability', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(9, 2, 'Non-current Liability', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(10, 3, 'Depreciation', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(11, 3, 'Direct Costs', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(12, 3, 'Expense', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(13, 4, 'Revenue', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(14, 4, 'Sales', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(15, 4, 'Other Income', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(16, 5, 'Equity', '2018-08-28 06:13:24', '2018-08-28 06:13:24', NULL),
(17, 1, 'Current Asset', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(18, 1, 'Fixed Asset', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(19, 1, 'Inventory', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(20, 1, 'Non-current Asset', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(21, 1, 'Prepayment', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(22, 1, 'Bank & Cash', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(23, 2, 'Current Liability', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(24, 2, 'Liability', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(25, 2, 'Non-current Liability', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(26, 3, 'Depreciation', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(27, 3, 'Direct Costs', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(28, 3, 'Expense', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(29, 4, 'Revenue', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(30, 4, 'Sales', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(31, 4, 'Other Income', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(32, 5, 'Equity', '2018-08-28 12:36:56', '2018-08-28 12:36:56', NULL),
(33, 1, 'Current Asset', '2018-08-28 12:37:42', '2018-08-28 12:37:42', NULL),
(34, 1, 'Fixed Asset', '2018-08-28 12:37:42', '2018-08-28 12:37:42', NULL),
(35, 1, 'Inventory', '2018-08-28 12:37:42', '2018-08-28 12:37:42', NULL),
(36, 1, 'Non-current Asset', '2018-08-28 12:37:42', '2018-08-28 12:37:42', NULL),
(37, 1, 'Prepayment', '2018-08-28 12:37:42', '2018-08-28 12:37:42', NULL),
(38, 1, 'Bank & Cash', '2018-08-28 12:37:42', '2018-08-28 12:37:42', NULL),
(39, 2, 'Current Liability', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(40, 2, 'Liability', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(41, 2, 'Non-current Liability', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(42, 3, 'Depreciation', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(43, 3, 'Direct Costs', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(44, 3, 'Expense', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(45, 4, 'Revenue', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(46, 4, 'Sales', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(47, 4, 'Other Income', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL),
(48, 5, 'Equity', '2018-08-28 12:37:43', '2018-08-28 12:37:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_failed_jobs`
--

CREATE TABLE `fss_failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_invoices`
--

CREATE TABLE `fss_invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_status_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoiced_at` datetime NOT NULL,
  `due_at` datetime NOT NULL,
  `amount` double(15,4) NOT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` double(15,8) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_tax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_address` text COLLATE utf8mb4_unicode_ci,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_invoice_histories`
--

CREATE TABLE `fss_invoice_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `status_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_invoice_items`
--

CREATE TABLE `fss_invoice_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` double(7,2) NOT NULL,
  `price` double(15,4) NOT NULL,
  `total` double(15,4) NOT NULL,
  `tax` double(15,4) NOT NULL DEFAULT '0.0000',
  `tax_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_invoice_payments`
--

CREATE TABLE `fss_invoice_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `paid_at` datetime NOT NULL,
  `amount` double(15,4) NOT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` double(15,8) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_invoice_statuses`
--

CREATE TABLE `fss_invoice_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_invoice_statuses`
--

INSERT INTO `fss_invoice_statuses` (`id`, `company_id`, `name`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Draft', 'draft', '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(2, 1, 'Sent', 'sent', '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(3, 1, 'Viewed', 'viewed', '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(4, 1, 'Approved', 'approved', '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(5, 1, 'Partial', 'partial', '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(6, 1, 'Paid', 'paid', '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_invoice_totals`
--

CREATE TABLE `fss_invoice_totals` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(15,4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_items`
--

CREATE TABLE `fss_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `sale_price` double(15,4) NOT NULL,
  `purchase_price` double(15,4) NOT NULL,
  `quantity` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_jobs`
--

CREATE TABLE `fss_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_media`
--

CREATE TABLE `fss_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `directory` varchar(68) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(121) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(28) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aggregate_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_mediables`
--

CREATE TABLE `fss_mediables` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `mediable_type` varchar(152) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mediable_id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(68) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_migrations`
--

CREATE TABLE `fss_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_migrations`
--

INSERT INTO `fss_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_09_01_000000_create_accounts_table', 1),
(2, '2017_09_01_000000_create_bills_table', 1),
(3, '2017_09_01_000000_create_categories_table', 1),
(4, '2017_09_01_000000_create_companies_table', 1),
(5, '2017_09_01_000000_create_currencies_table', 1),
(6, '2017_09_01_000000_create_customers_table', 1),
(7, '2017_09_01_000000_create_invoices_table', 1),
(8, '2017_09_01_000000_create_items_table', 1),
(9, '2017_09_01_000000_create_jobs_table', 1),
(10, '2017_09_01_000000_create_modules_table', 1),
(11, '2017_09_01_000000_create_notifications_table', 1),
(12, '2017_09_01_000000_create_password_resets_table', 1),
(13, '2017_09_01_000000_create_payments_table', 1),
(14, '2017_09_01_000000_create_revenues_table', 1),
(15, '2017_09_01_000000_create_roles_table', 1),
(16, '2017_09_01_000000_create_sessions_table', 1),
(17, '2017_09_01_000000_create_settings_table', 1),
(18, '2017_09_01_000000_create_taxes_table', 1),
(19, '2017_09_01_000000_create_transfers_table', 1),
(20, '2017_09_01_000000_create_users_table', 1),
(21, '2017_09_01_000000_create_vendors_table', 1),
(22, '2017_09_19_delete_offline_file', 1),
(23, '2017_10_11_000000_create_bill_totals_table', 1),
(24, '2017_10_11_000000_create_invoice_totals_table', 1),
(25, '2017_11_16_000000_create_failed_jobs_table', 1),
(26, '2017_12_09_000000_add_currency_columns', 1),
(27, '2017_12_30_000000_create_mediable_tables', 1),
(28, '2018_01_03_000000_drop_attachment_column_bill_payments_table', 1),
(29, '2018_01_03_000000_drop_attachment_column_bills_table', 1),
(30, '2018_01_03_000000_drop_attachment_column_invoice_payments_table', 1),
(31, '2018_01_03_000000_drop_attachment_column_invoices_table', 1),
(32, '2018_01_03_000000_drop_attachment_column_payments_table', 1),
(33, '2018_01_03_000000_drop_attachment_column_revenues_table', 1),
(34, '2018_01_03_000000_drop_picture_column_items_table', 1),
(35, '2018_01_03_000000_drop_picture_column_users_table', 1),
(36, '2018_04_23_000000_add_category_column_invoices_bills', 1),
(37, '2018_04_26_000000_create_recurring_table', 1),
(38, '2018_04_30_000000_add_parent_column', 1),
(39, '2018_06_23_000000_modify_email_column', 1),
(40, '2018_06_30_000000_modify_enabled_column', 1),
(41, '2018_07_07_000000_modify_date_column', 1),
(42, '2020_01_01_000000_add_locale_column', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fss_modules`
--

CREATE TABLE `fss_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_modules`
--

INSERT INTO `fss_modules` (`id`, `company_id`, `alias`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'offlinepayment', 1, '2018-08-28 11:04:57', '2018-08-28 11:04:57', NULL),
(2, 1, 'paypalstandard', 1, '2018-08-28 11:04:58', '2018-08-28 11:04:58', NULL),
(3, 1, 'double-entry', 1, '2018-08-28 11:10:17', '2018-08-28 11:10:17', NULL),
(4, 1, 'double-entry', 1, '2018-08-28 11:11:47', '2018-08-28 11:11:47', NULL),
(5, 1, 'double-entry', 1, '2018-08-21 19:30:00', '2018-08-21 19:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_module_histories`
--

CREATE TABLE `fss_module_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_module_histories`
--

INSERT INTO `fss_module_histories` (`id`, `company_id`, `module_id`, `category`, `version`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'payment-gateways', '1.0.0', 'OfflinePayment installed', '2018-08-28 11:04:58', '2018-08-28 11:04:58', NULL),
(2, 1, 2, 'payment-gateways', '1.0.0', 'PaypalStandard installed', '2018-08-28 11:04:58', '2018-08-28 11:04:58', NULL),
(3, 1, 0, 'accounting', '1.0.4', 'Double entry accounting', '2018-08-21 19:30:00', '2018-08-21 19:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_notifications`
--

CREATE TABLE `fss_notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_password_resets`
--

CREATE TABLE `fss_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_payments`
--

CREATE TABLE `fss_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `paid_at` datetime NOT NULL,
  `amount` double(15,4) NOT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` double(15,8) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_permissions`
--

CREATE TABLE `fss_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_permissions`
--

INSERT INTO `fss_permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'read-admin-panel', 'Read Admin Panel', 'Read Admin Panel', '2018-08-28 11:03:52', '2018-08-28 11:03:52'),
(2, 'read-api', 'Read Api', 'Read Api', '2018-08-28 11:03:52', '2018-08-28 11:03:52'),
(3, 'create-auth-users', 'Create Auth Users', 'Create Auth Users', '2018-08-28 11:03:52', '2018-08-28 11:03:52'),
(4, 'read-auth-users', 'Read Auth Users', 'Read Auth Users', '2018-08-28 11:03:52', '2018-08-28 11:03:52'),
(5, 'update-auth-users', 'Update Auth Users', 'Update Auth Users', '2018-08-28 11:03:52', '2018-08-28 11:03:52'),
(6, 'delete-auth-users', 'Delete Auth Users', 'Delete Auth Users', '2018-08-28 11:03:53', '2018-08-28 11:03:53'),
(7, 'create-auth-roles', 'Create Auth Roles', 'Create Auth Roles', '2018-08-28 11:03:53', '2018-08-28 11:03:53'),
(8, 'read-auth-roles', 'Read Auth Roles', 'Read Auth Roles', '2018-08-28 11:03:53', '2018-08-28 11:03:53'),
(9, 'update-auth-roles', 'Update Auth Roles', 'Update Auth Roles', '2018-08-28 11:03:53', '2018-08-28 11:03:53'),
(10, 'delete-auth-roles', 'Delete Auth Roles', 'Delete Auth Roles', '2018-08-28 11:03:53', '2018-08-28 11:03:53'),
(11, 'create-auth-permissions', 'Create Auth Permissions', 'Create Auth Permissions', '2018-08-28 11:03:54', '2018-08-28 11:03:54'),
(12, 'read-auth-permissions', 'Read Auth Permissions', 'Read Auth Permissions', '2018-08-28 11:03:54', '2018-08-28 11:03:54'),
(13, 'update-auth-permissions', 'Update Auth Permissions', 'Update Auth Permissions', '2018-08-28 11:03:54', '2018-08-28 11:03:54'),
(14, 'delete-auth-permissions', 'Delete Auth Permissions', 'Delete Auth Permissions', '2018-08-28 11:03:54', '2018-08-28 11:03:54'),
(15, 'read-auth-profile', 'Read Auth Profile', 'Read Auth Profile', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(16, 'update-auth-profile', 'Update Auth Profile', 'Update Auth Profile', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(17, 'create-common-companies', 'Create Common Companies', 'Create Common Companies', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(18, 'read-common-companies', 'Read Common Companies', 'Read Common Companies', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(19, 'update-common-companies', 'Update Common Companies', 'Update Common Companies', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(20, 'delete-common-companies', 'Delete Common Companies', 'Delete Common Companies', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(21, 'create-common-import', 'Create Common Import', 'Create Common Import', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(22, 'create-common-items', 'Create Common Items', 'Create Common Items', '2018-08-28 11:03:55', '2018-08-28 11:03:55'),
(23, 'read-common-items', 'Read Common Items', 'Read Common Items', '2018-08-28 11:03:56', '2018-08-28 11:03:56'),
(24, 'update-common-items', 'Update Common Items', 'Update Common Items', '2018-08-28 11:03:56', '2018-08-28 11:03:56'),
(25, 'delete-common-items', 'Delete Common Items', 'Delete Common Items', '2018-08-28 11:03:56', '2018-08-28 11:03:56'),
(26, 'delete-common-uploads', 'Delete Common Uploads', 'Delete Common Uploads', '2018-08-28 11:03:56', '2018-08-28 11:03:56'),
(27, 'create-incomes-invoices', 'Create Incomes Invoices', 'Create Incomes Invoices', '2018-08-28 11:03:56', '2018-08-28 11:03:56'),
(28, 'read-incomes-invoices', 'Read Incomes Invoices', 'Read Incomes Invoices', '2018-08-28 11:03:56', '2018-08-28 11:03:56'),
(29, 'update-incomes-invoices', 'Update Incomes Invoices', 'Update Incomes Invoices', '2018-08-28 11:03:56', '2018-08-28 11:03:56'),
(30, 'delete-incomes-invoices', 'Delete Incomes Invoices', 'Delete Incomes Invoices', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(31, 'create-incomes-revenues', 'Create Incomes Revenues', 'Create Incomes Revenues', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(32, 'read-incomes-revenues', 'Read Incomes Revenues', 'Read Incomes Revenues', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(33, 'update-incomes-revenues', 'Update Incomes Revenues', 'Update Incomes Revenues', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(34, 'delete-incomes-revenues', 'Delete Incomes Revenues', 'Delete Incomes Revenues', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(35, 'create-incomes-customers', 'Create Incomes Customers', 'Create Incomes Customers', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(36, 'read-incomes-customers', 'Read Incomes Customers', 'Read Incomes Customers', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(37, 'update-incomes-customers', 'Update Incomes Customers', 'Update Incomes Customers', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(38, 'delete-incomes-customers', 'Delete Incomes Customers', 'Delete Incomes Customers', '2018-08-28 11:03:57', '2018-08-28 11:03:57'),
(39, 'create-expenses-bills', 'Create Expenses Bills', 'Create Expenses Bills', '2018-08-28 11:03:58', '2018-08-28 11:03:58'),
(40, 'read-expenses-bills', 'Read Expenses Bills', 'Read Expenses Bills', '2018-08-28 11:03:58', '2018-08-28 11:03:58'),
(41, 'update-expenses-bills', 'Update Expenses Bills', 'Update Expenses Bills', '2018-08-28 11:03:58', '2018-08-28 11:03:58'),
(42, 'delete-expenses-bills', 'Delete Expenses Bills', 'Delete Expenses Bills', '2018-08-28 11:03:58', '2018-08-28 11:03:58'),
(43, 'create-expenses-payments', 'Create Expenses Payments', 'Create Expenses Payments', '2018-08-28 11:03:58', '2018-08-28 11:03:58'),
(44, 'read-expenses-payments', 'Read Expenses Payments', 'Read Expenses Payments', '2018-08-28 11:03:58', '2018-08-28 11:03:58'),
(45, 'update-expenses-payments', 'Update Expenses Payments', 'Update Expenses Payments', '2018-08-28 11:03:58', '2018-08-28 11:03:58'),
(46, 'delete-expenses-payments', 'Delete Expenses Payments', 'Delete Expenses Payments', '2018-08-28 11:03:59', '2018-08-28 11:03:59'),
(47, 'create-expenses-vendors', 'Create Expenses Vendors', 'Create Expenses Vendors', '2018-08-28 11:03:59', '2018-08-28 11:03:59'),
(48, 'read-expenses-vendors', 'Read Expenses Vendors', 'Read Expenses Vendors', '2018-08-28 11:03:59', '2018-08-28 11:03:59'),
(49, 'update-expenses-vendors', 'Update Expenses Vendors', 'Update Expenses Vendors', '2018-08-28 11:03:59', '2018-08-28 11:03:59'),
(50, 'delete-expenses-vendors', 'Delete Expenses Vendors', 'Delete Expenses Vendors', '2018-08-28 11:03:59', '2018-08-28 11:03:59'),
(51, 'create-banking-accounts', 'Create Banking Accounts', 'Create Banking Accounts', '2018-08-28 11:04:00', '2018-08-28 11:04:00'),
(52, 'read-banking-accounts', 'Read Banking Accounts', 'Read Banking Accounts', '2018-08-28 11:04:00', '2018-08-28 11:04:00'),
(53, 'update-banking-accounts', 'Update Banking Accounts', 'Update Banking Accounts', '2018-08-28 11:04:00', '2018-08-28 11:04:00'),
(54, 'delete-banking-accounts', 'Delete Banking Accounts', 'Delete Banking Accounts', '2018-08-28 11:04:00', '2018-08-28 11:04:00'),
(55, 'create-banking-transfers', 'Create Banking Transfers', 'Create Banking Transfers', '2018-08-28 11:04:00', '2018-08-28 11:04:00'),
(56, 'read-banking-transfers', 'Read Banking Transfers', 'Read Banking Transfers', '2018-08-28 11:04:00', '2018-08-28 11:04:00'),
(57, 'update-banking-transfers', 'Update Banking Transfers', 'Update Banking Transfers', '2018-08-28 11:04:01', '2018-08-28 11:04:01'),
(58, 'delete-banking-transfers', 'Delete Banking Transfers', 'Delete Banking Transfers', '2018-08-28 11:04:01', '2018-08-28 11:04:01'),
(59, 'read-banking-transactions', 'Read Banking Transactions', 'Read Banking Transactions', '2018-08-28 11:04:01', '2018-08-28 11:04:01'),
(60, 'create-settings-categories', 'Create Settings Categories', 'Create Settings Categories', '2018-08-28 11:04:01', '2018-08-28 11:04:01'),
(61, 'read-settings-categories', 'Read Settings Categories', 'Read Settings Categories', '2018-08-28 11:04:01', '2018-08-28 11:04:01'),
(62, 'update-settings-categories', 'Update Settings Categories', 'Update Settings Categories', '2018-08-28 11:04:01', '2018-08-28 11:04:01'),
(63, 'delete-settings-categories', 'Delete Settings Categories', 'Delete Settings Categories', '2018-08-28 11:04:02', '2018-08-28 11:04:02'),
(64, 'read-settings-settings', 'Read Settings Settings', 'Read Settings Settings', '2018-08-28 11:04:02', '2018-08-28 11:04:02'),
(65, 'update-settings-settings', 'Update Settings Settings', 'Update Settings Settings', '2018-08-28 11:04:02', '2018-08-28 11:04:02'),
(66, 'create-settings-taxes', 'Create Settings Taxes', 'Create Settings Taxes', '2018-08-28 11:04:02', '2018-08-28 11:04:02'),
(67, 'read-settings-taxes', 'Read Settings Taxes', 'Read Settings Taxes', '2018-08-28 11:04:03', '2018-08-28 11:04:03'),
(68, 'update-settings-taxes', 'Update Settings Taxes', 'Update Settings Taxes', '2018-08-28 11:04:03', '2018-08-28 11:04:03'),
(69, 'delete-settings-taxes', 'Delete Settings Taxes', 'Delete Settings Taxes', '2018-08-28 11:04:03', '2018-08-28 11:04:03'),
(70, 'create-settings-currencies', 'Create Settings Currencies', 'Create Settings Currencies', '2018-08-28 11:04:03', '2018-08-28 11:04:03'),
(71, 'read-settings-currencies', 'Read Settings Currencies', 'Read Settings Currencies', '2018-08-28 11:04:03', '2018-08-28 11:04:03'),
(72, 'update-settings-currencies', 'Update Settings Currencies', 'Update Settings Currencies', '2018-08-28 11:04:03', '2018-08-28 11:04:03'),
(73, 'delete-settings-currencies', 'Delete Settings Currencies', 'Delete Settings Currencies', '2018-08-28 11:04:04', '2018-08-28 11:04:04'),
(74, 'read-settings-modules', 'Read Settings Modules', 'Read Settings Modules', '2018-08-28 11:04:04', '2018-08-28 11:04:04'),
(75, 'update-settings-modules', 'Update Settings Modules', 'Update Settings Modules', '2018-08-28 11:04:04', '2018-08-28 11:04:04'),
(76, 'read-modules-home', 'Read Modules Home', 'Read Modules Home', '2018-08-28 11:04:04', '2018-08-28 11:04:04'),
(77, 'read-modules-tiles', 'Read Modules Tiles', 'Read Modules Tiles', '2018-08-28 11:04:04', '2018-08-28 11:04:04'),
(78, 'create-modules-item', 'Create Modules Item', 'Create Modules Item', '2018-08-28 11:04:05', '2018-08-28 11:04:05'),
(79, 'read-modules-item', 'Read Modules Item', 'Read Modules Item', '2018-08-28 11:04:05', '2018-08-28 11:04:05'),
(80, 'update-modules-item', 'Update Modules Item', 'Update Modules Item', '2018-08-28 11:04:05', '2018-08-28 11:04:05'),
(81, 'delete-modules-item', 'Delete Modules Item', 'Delete Modules Item', '2018-08-28 11:04:05', '2018-08-28 11:04:05'),
(82, 'create-modules-token', 'Create Modules Token', 'Create Modules Token', '2018-08-28 11:04:05', '2018-08-28 11:04:05'),
(83, 'update-modules-token', 'Update Modules Token', 'Update Modules Token', '2018-08-28 11:04:05', '2018-08-28 11:04:05'),
(84, 'read-modules-my', 'Read Modules My', 'Read Modules My', '2018-08-28 11:04:05', '2018-08-28 11:04:05'),
(85, 'read-install-updates', 'Read Install Updates', 'Read Install Updates', '2018-08-28 11:04:06', '2018-08-28 11:04:06'),
(86, 'update-install-updates', 'Update Install Updates', 'Update Install Updates', '2018-08-28 11:04:06', '2018-08-28 11:04:06'),
(87, 'read-notifications', 'Read Notifications', 'Read Notifications', '2018-08-28 11:04:06', '2018-08-28 11:04:06'),
(88, 'update-notifications', 'Update Notifications', 'Update Notifications', '2018-08-28 11:04:06', '2018-08-28 11:04:06'),
(89, 'read-reports-income-summary', 'Read Reports Income Summary', 'Read Reports Income Summary', '2018-08-28 11:04:06', '2018-08-28 11:04:06'),
(90, 'read-reports-expense-summary', 'Read Reports Expense Summary', 'Read Reports Expense Summary', '2018-08-28 11:04:07', '2018-08-28 11:04:07'),
(91, 'read-reports-income-expense-summary', 'Read Reports Income Expense Summary', 'Read Reports Income Expense Summary', '2018-08-28 11:04:07', '2018-08-28 11:04:07'),
(92, 'read-reports-profit-loss', 'Read Reports Profit Loss', 'Read Reports Profit Loss', '2018-08-28 11:04:07', '2018-08-28 11:04:07'),
(93, 'read-reports-tax-summary', 'Read Reports Tax Summary', 'Read Reports Tax Summary', '2018-08-28 11:04:07', '2018-08-28 11:04:07'),
(94, 'read-customer-panel', 'Read Customer Panel', 'Read Customer Panel', '2018-08-28 11:04:12', '2018-08-28 11:04:12'),
(95, 'read-customers-invoices', 'Read Customers Invoices', 'Read Customers Invoices', '2018-08-28 11:04:12', '2018-08-28 11:04:12'),
(96, 'update-customers-invoices', 'Update Customers Invoices', 'Update Customers Invoices', '2018-08-28 11:04:12', '2018-08-28 11:04:12'),
(97, 'read-customers-payments', 'Read Customers Payments', 'Read Customers Payments', '2018-08-28 11:04:12', '2018-08-28 11:04:12'),
(98, 'update-customers-payments', 'Update Customers Payments', 'Update Customers Payments', '2018-08-28 11:04:12', '2018-08-28 11:04:12'),
(99, 'read-customers-transactions', 'Read Customers Transactions', 'Read Customers Transactions', '2018-08-28 11:04:12', '2018-08-28 11:04:12'),
(100, 'read-customers-profile', 'Read Customers Profile', 'Read Customers Profile', '2018-08-28 11:04:13', '2018-08-28 11:04:13'),
(101, 'update-customers-profile', 'Update Customers Profile', 'Update Customers Profile', '2018-08-28 11:04:13', '2018-08-28 11:04:13'),
(108, 'read-double-entry-balance-sheet', 'Read Double-Entry Balance Sheet', 'Read Double-Entry Balance Sheet', '2018-08-21 19:30:00', '2018-08-21 19:30:00'),
(109, 'create-double-entry-chart-of-accounts', 'Create Double-Entry Chart of Accounts', 'Create Double-Entry Chart of Accounts', '2018-08-21 19:30:00', '2018-08-21 19:30:00'),
(110, 'read-double-entry-chart-of-accounts', 'Read Double-Entry Chart of Accounts', 'Read Double-Entry Chart of Accounts', '2018-08-21 19:30:00', '2018-08-21 19:30:00'),
(111, 'update-double-entry-chart-of-accounts', 'Update Double-Entry Chart of Accounts', 'Update Double-Entry Chart of Accounts', '2018-08-21 19:30:00', '2018-08-21 19:30:00'),
(112, 'delete-double-entry-chart-of-accounts', 'Delete Double-Entry Chart of Accounts', 'Delete Double-Entry Chart of Accounts', NULL, NULL),
(113, 'read-double-entry-general-ledger', 'Read Double-Entry General Ledger', 'Read Double-Entry General Ledger', NULL, NULL),
(114, 'create-double-entry-journal-entry', 'Create Double-Entry Journal Entry', 'Create Double-Entry Journal Entry', NULL, NULL),
(115, 'read-double-entry-journal-entry', 'Read Double-Entry Journal Entry', 'Read Double-Entry Journal Entry', '2018-08-21 18:30:00', '2018-08-27 18:30:00'),
(116, 'update-double-entry-journal-entry', 'Update Double-Entry Journal Entry', 'Update Double-Entry Journal Entry', NULL, NULL),
(117, 'delete-double-entry-journal-entry', 'Delete Double-Entry Journal Entry', 'Delete Double-Entry Journal Entry', NULL, NULL),
(118, 'read-double-entry-trial-balance', 'Read Double-Entry Trial Balance', 'Read Double-Entry Trial Balance', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_recurring`
--

CREATE TABLE `fss_recurring` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `recurable_id` int(10) UNSIGNED NOT NULL,
  `recurable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frequency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interval` int(11) NOT NULL DEFAULT '1',
  `started_at` datetime NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_revenues`
--

CREATE TABLE `fss_revenues` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `paid_at` datetime NOT NULL,
  `amount` double(15,4) NOT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` double(15,8) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_roles`
--

CREATE TABLE `fss_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_roles`
--

INSERT INTO `fss_roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', 'Admin', '2018-08-28 11:03:51', '2018-08-28 11:03:51'),
(2, 'manager', 'Manager', 'Manager', '2018-08-28 11:04:07', '2018-08-28 11:04:07'),
(3, 'customer', 'Customer', 'Customer', '2018-08-28 11:04:12', '2018-08-28 11:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `fss_role_permissions`
--

CREATE TABLE `fss_role_permissions` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_role_permissions`
--

INSERT INTO `fss_role_permissions` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 72),
(1, 73),
(1, 74),
(1, 75),
(1, 76),
(1, 77),
(1, 78),
(1, 79),
(1, 80),
(1, 81),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 108),
(1, 109),
(1, 110),
(1, 111),
(1, 112),
(1, 113),
(1, 114),
(1, 115),
(1, 116),
(1, 117),
(1, 118),
(2, 1),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49),
(2, 50),
(2, 51),
(2, 52),
(2, 53),
(2, 54),
(2, 55),
(2, 56),
(2, 57),
(2, 58),
(2, 59),
(2, 60),
(2, 61),
(2, 62),
(2, 63),
(2, 64),
(2, 65),
(2, 66),
(2, 67),
(2, 68),
(2, 69),
(2, 70),
(2, 71),
(2, 72),
(2, 73),
(2, 74),
(2, 75),
(2, 85),
(2, 86),
(2, 87),
(2, 88),
(2, 89),
(2, 90),
(2, 91),
(2, 92),
(2, 93),
(3, 94),
(3, 95),
(3, 96),
(3, 97),
(3, 98),
(3, 99),
(3, 100),
(3, 101);

-- --------------------------------------------------------

--
-- Table structure for table `fss_sessions`
--

CREATE TABLE `fss_sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_settings`
--

CREATE TABLE `fss_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_settings`
--

INSERT INTO `fss_settings` (`id`, `company_id`, `key`, `value`) VALUES
(1, 1, 'general.default_account', '1'),
(2, 1, 'general.date_format', 'd M Y'),
(3, 1, 'general.date_separator', 'space'),
(4, 1, 'general.timezone', 'Europe/London'),
(5, 1, 'general.percent_position', 'after'),
(6, 1, 'general.invoice_number_prefix', 'INV-'),
(7, 1, 'general.invoice_number_digit', '5'),
(8, 1, 'general.invoice_number_next', '1'),
(9, 1, 'general.default_payment_method', 'offlinepayment.cash.1'),
(10, 1, 'general.email_protocol', 'mail'),
(11, 1, 'general.email_sendmail_path', '/usr/sbin/sendmail -bs'),
(12, 1, 'general.send_invoice_reminder', '0'),
(13, 1, 'general.schedule_invoice_days', '1,3,5,10'),
(14, 1, 'general.send_bill_reminder', '0'),
(15, 1, 'general.schedule_bill_days', '10,5,3,1'),
(16, 1, 'general.schedule_time', '09:00'),
(17, 1, 'general.admin_theme', 'skin-green-light'),
(18, 1, 'general.list_limit', '25'),
(19, 1, 'general.use_gravatar', '0'),
(20, 1, 'general.session_handler', 'file'),
(21, 1, 'general.session_lifetime', '30'),
(22, 1, 'general.file_size', '2'),
(23, 1, 'general.file_types', 'pdf,jpeg,jpg,png'),
(24, 1, 'general.company_name', 'Jaswinder'),
(25, 1, 'general.company_email', 'maanjaswinder2412@gmail.com'),
(26, 1, 'general.default_currency', 'USD'),
(27, 1, 'general.default_locale', 'en-GB'),
(28, 1, 'offlinepayment.methods', '[{\"code\":\"offlinepayment.cash.1\",\"name\":\"Cash\",\"order\":\"1\",\"description\":null},{\"code\":\"offlinepayment.bank_transfer.2\",\"name\":\"Bank Transfer\",\"order\":\"2\",\"description\":null}]');

-- --------------------------------------------------------

--
-- Table structure for table `fss_taxes`
--

CREATE TABLE `fss_taxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double(15,4) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_taxes`
--

INSERT INTO `fss_taxes` (`id`, `company_id`, `name`, `rate`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Tax Exempt', 0.0000, 1, '2018-08-28 11:04:58', '2018-08-28 11:04:58', NULL),
(2, 1, 'Normal Tax', 5.0000, 1, '2018-08-28 11:04:58', '2018-08-28 11:04:58', NULL),
(3, 1, 'Sales Tax', 15.0000, 1, '2018-08-28 11:04:58', '2018-08-28 11:04:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fss_transfers`
--

CREATE TABLE `fss_transfers` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `revenue_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_users`
--

CREATE TABLE `fss_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_logged_in_at` timestamp NULL DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en-GB'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_users`
--

INSERT INTO `fss_users` (`id`, `name`, `email`, `password`, `remember_token`, `last_logged_in_at`, `enabled`, `created_at`, `updated_at`, `deleted_at`, `locale`) VALUES
(1, '', 'admin@nomail.com', '$2y$10$vkF6E5SyEeUpDzg7RJZm3eQ2/EJ7hu46s/IRoUOPPJNiU8AJSM/Te', NULL, '2018-08-28 12:53:32', 1, '2018-08-28 11:04:59', '2018-08-28 12:53:32', NULL, 'en-GB');

-- --------------------------------------------------------

--
-- Table structure for table `fss_user_companies`
--

CREATE TABLE `fss_user_companies` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_user_companies`
--

INSERT INTO `fss_user_companies` (`user_id`, `company_id`, `user_type`) VALUES
(1, 1, 'users');

-- --------------------------------------------------------

--
-- Table structure for table `fss_user_permissions`
--

CREATE TABLE `fss_user_permissions` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fss_user_roles`
--

CREATE TABLE `fss_user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fss_user_roles`
--

INSERT INTO `fss_user_roles` (`user_id`, `role_id`, `user_type`) VALUES
(1, 1, 'users');

-- --------------------------------------------------------

--
-- Table structure for table `fss_vendors`
--

CREATE TABLE `fss_vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fss_accounts`
--
ALTER TABLE `fss_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accounts_company_id_index` (`company_id`);

--
-- Indexes for table `fss_bills`
--
ALTER TABLE `fss_bills`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bills_company_id_bill_number_deleted_at_unique` (`company_id`,`bill_number`,`deleted_at`),
  ADD KEY `bills_company_id_index` (`company_id`);

--
-- Indexes for table `fss_bill_histories`
--
ALTER TABLE `fss_bill_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_histories_company_id_index` (`company_id`);

--
-- Indexes for table `fss_bill_items`
--
ALTER TABLE `fss_bill_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_items_company_id_index` (`company_id`);

--
-- Indexes for table `fss_bill_payments`
--
ALTER TABLE `fss_bill_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_payments_company_id_index` (`company_id`);

--
-- Indexes for table `fss_bill_statuses`
--
ALTER TABLE `fss_bill_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_statuses_company_id_index` (`company_id`);

--
-- Indexes for table `fss_bill_totals`
--
ALTER TABLE `fss_bill_totals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_totals_company_id_index` (`company_id`);

--
-- Indexes for table `fss_categories`
--
ALTER TABLE `fss_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_company_id_index` (`company_id`);

--
-- Indexes for table `fss_companies`
--
ALTER TABLE `fss_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fss_currencies`
--
ALTER TABLE `fss_currencies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `currencies_company_id_code_deleted_at_unique` (`company_id`,`code`,`deleted_at`),
  ADD KEY `currencies_company_id_index` (`company_id`);

--
-- Indexes for table `fss_customers`
--
ALTER TABLE `fss_customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_company_id_email_deleted_at_unique` (`company_id`,`email`,`deleted_at`),
  ADD KEY `customers_company_id_index` (`company_id`);

--
-- Indexes for table `fss_double_entry_accounts`
--
ALTER TABLE `fss_double_entry_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `double_entry_accounts_company_id_index` (`company_id`),
  ADD KEY `double_entry_accounts_type_id_index` (`type_id`);

--
-- Indexes for table `fss_double_entry_account_bank`
--
ALTER TABLE `fss_double_entry_account_bank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `double_entry_account_bank_company_id_index` (`company_id`);

--
-- Indexes for table `fss_double_entry_classes`
--
ALTER TABLE `fss_double_entry_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fss_double_entry_journals`
--
ALTER TABLE `fss_double_entry_journals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `double_entry_journals_company_id_index` (`company_id`);

--
-- Indexes for table `fss_double_entry_ledger`
--
ALTER TABLE `fss_double_entry_ledger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `double_entry_ledger_ledgerable_id_ledgerable_type_index` (`ledgerable_id`,`ledgerable_type`),
  ADD KEY `double_entry_ledger_company_id_index` (`company_id`),
  ADD KEY `double_entry_ledger_account_id_index` (`account_id`);

--
-- Indexes for table `fss_double_entry_types`
--
ALTER TABLE `fss_double_entry_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `double_entry_types_class_id_index` (`class_id`);

--
-- Indexes for table `fss_failed_jobs`
--
ALTER TABLE `fss_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fss_invoices`
--
ALTER TABLE `fss_invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_company_id_invoice_number_deleted_at_unique` (`company_id`,`invoice_number`,`deleted_at`),
  ADD KEY `invoices_company_id_index` (`company_id`);

--
-- Indexes for table `fss_invoice_histories`
--
ALTER TABLE `fss_invoice_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_histories_company_id_index` (`company_id`);

--
-- Indexes for table `fss_invoice_items`
--
ALTER TABLE `fss_invoice_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_items_company_id_index` (`company_id`);

--
-- Indexes for table `fss_invoice_payments`
--
ALTER TABLE `fss_invoice_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_payments_company_id_index` (`company_id`);

--
-- Indexes for table `fss_invoice_statuses`
--
ALTER TABLE `fss_invoice_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_statuses_company_id_index` (`company_id`);

--
-- Indexes for table `fss_invoice_totals`
--
ALTER TABLE `fss_invoice_totals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_totals_company_id_index` (`company_id`);

--
-- Indexes for table `fss_items`
--
ALTER TABLE `fss_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `items_company_id_sku_deleted_at_unique` (`company_id`,`sku`,`deleted_at`),
  ADD KEY `items_company_id_index` (`company_id`);

--
-- Indexes for table `fss_jobs`
--
ALTER TABLE `fss_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `fss_media`
--
ALTER TABLE `fss_media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `media_disk_directory_filename_extension_unique` (`disk`,`directory`,`filename`,`extension`),
  ADD KEY `media_disk_directory_index` (`disk`,`directory`),
  ADD KEY `media_aggregate_type_index` (`aggregate_type`);

--
-- Indexes for table `fss_mediables`
--
ALTER TABLE `fss_mediables`
  ADD PRIMARY KEY (`media_id`,`mediable_type`,`mediable_id`,`tag`),
  ADD KEY `mediables_mediable_id_mediable_type_index` (`mediable_id`,`mediable_type`),
  ADD KEY `mediables_tag_index` (`tag`),
  ADD KEY `mediables_order_index` (`order`);

--
-- Indexes for table `fss_migrations`
--
ALTER TABLE `fss_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fss_modules`
--
ALTER TABLE `fss_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `modules_company_id_alias_deleted_at_unique` (`company_id`,`alias`,`deleted_at`),
  ADD KEY `modules_company_id_index` (`company_id`);

--
-- Indexes for table `fss_module_histories`
--
ALTER TABLE `fss_module_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_histories_company_id_module_id_index` (`company_id`,`module_id`);

--
-- Indexes for table `fss_notifications`
--
ALTER TABLE `fss_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indexes for table `fss_password_resets`
--
ALTER TABLE `fss_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `fss_payments`
--
ALTER TABLE `fss_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_company_id_index` (`company_id`);

--
-- Indexes for table `fss_permissions`
--
ALTER TABLE `fss_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `fss_recurring`
--
ALTER TABLE `fss_recurring`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recurring_recurable_id_recurable_type_index` (`recurable_id`,`recurable_type`);

--
-- Indexes for table `fss_revenues`
--
ALTER TABLE `fss_revenues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `revenues_company_id_index` (`company_id`);

--
-- Indexes for table `fss_roles`
--
ALTER TABLE `fss_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `fss_role_permissions`
--
ALTER TABLE `fss_role_permissions`
  ADD PRIMARY KEY (`role_id`,`permission_id`),
  ADD KEY `role_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `fss_sessions`
--
ALTER TABLE `fss_sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `fss_settings`
--
ALTER TABLE `fss_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_company_id_key_unique` (`company_id`,`key`),
  ADD KEY `settings_company_id_index` (`company_id`);

--
-- Indexes for table `fss_taxes`
--
ALTER TABLE `fss_taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taxes_company_id_index` (`company_id`);

--
-- Indexes for table `fss_transfers`
--
ALTER TABLE `fss_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transfers_company_id_index` (`company_id`);

--
-- Indexes for table `fss_users`
--
ALTER TABLE `fss_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_deleted_at_unique` (`email`,`deleted_at`);

--
-- Indexes for table `fss_user_companies`
--
ALTER TABLE `fss_user_companies`
  ADD PRIMARY KEY (`user_id`,`company_id`,`user_type`);

--
-- Indexes for table `fss_user_permissions`
--
ALTER TABLE `fss_user_permissions`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `user_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `fss_user_roles`
--
ALTER TABLE `fss_user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `fss_vendors`
--
ALTER TABLE `fss_vendors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendors_company_id_email_deleted_at_unique` (`company_id`,`email`,`deleted_at`),
  ADD KEY `vendors_company_id_index` (`company_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fss_accounts`
--
ALTER TABLE `fss_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fss_bills`
--
ALTER TABLE `fss_bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_bill_histories`
--
ALTER TABLE `fss_bill_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_bill_items`
--
ALTER TABLE `fss_bill_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_bill_payments`
--
ALTER TABLE `fss_bill_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_bill_statuses`
--
ALTER TABLE `fss_bill_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fss_bill_totals`
--
ALTER TABLE `fss_bill_totals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_categories`
--
ALTER TABLE `fss_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fss_companies`
--
ALTER TABLE `fss_companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fss_currencies`
--
ALTER TABLE `fss_currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fss_customers`
--
ALTER TABLE `fss_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_double_entry_accounts`
--
ALTER TABLE `fss_double_entry_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `fss_double_entry_account_bank`
--
ALTER TABLE `fss_double_entry_account_bank`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_double_entry_classes`
--
ALTER TABLE `fss_double_entry_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fss_double_entry_journals`
--
ALTER TABLE `fss_double_entry_journals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_double_entry_ledger`
--
ALTER TABLE `fss_double_entry_ledger`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_double_entry_types`
--
ALTER TABLE `fss_double_entry_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `fss_failed_jobs`
--
ALTER TABLE `fss_failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_invoices`
--
ALTER TABLE `fss_invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_invoice_histories`
--
ALTER TABLE `fss_invoice_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_invoice_items`
--
ALTER TABLE `fss_invoice_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_invoice_payments`
--
ALTER TABLE `fss_invoice_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_invoice_statuses`
--
ALTER TABLE `fss_invoice_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fss_invoice_totals`
--
ALTER TABLE `fss_invoice_totals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_items`
--
ALTER TABLE `fss_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_jobs`
--
ALTER TABLE `fss_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_media`
--
ALTER TABLE `fss_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_migrations`
--
ALTER TABLE `fss_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `fss_modules`
--
ALTER TABLE `fss_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fss_module_histories`
--
ALTER TABLE `fss_module_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fss_payments`
--
ALTER TABLE `fss_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_permissions`
--
ALTER TABLE `fss_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `fss_recurring`
--
ALTER TABLE `fss_recurring`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_revenues`
--
ALTER TABLE `fss_revenues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_roles`
--
ALTER TABLE `fss_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fss_settings`
--
ALTER TABLE `fss_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `fss_taxes`
--
ALTER TABLE `fss_taxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fss_transfers`
--
ALTER TABLE `fss_transfers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fss_users`
--
ALTER TABLE `fss_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fss_vendors`
--
ALTER TABLE `fss_vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fss_mediables`
--
ALTER TABLE `fss_mediables`
  ADD CONSTRAINT `mediables_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `fss_media` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fss_role_permissions`
--
ALTER TABLE `fss_role_permissions`
  ADD CONSTRAINT `role_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `fss_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `fss_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fss_user_permissions`
--
ALTER TABLE `fss_user_permissions`
  ADD CONSTRAINT `user_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `fss_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fss_user_roles`
--
ALTER TABLE `fss_user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `fss_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
